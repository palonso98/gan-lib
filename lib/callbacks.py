import tensorflow as tf
import time

class QualityCheckPlotCallback(tf.keras.callbacks.Callback):
    def __init__(self, val_batch=None, imagelabel="quality_check", hypar=None, step=tf.summary.experimental.get_step(), **kwargs):
        self.val_batch = val_batch
        self.hypar = hypar
        self.imagelabel = imagelabel

        self.kwargs = kwargs
        self.step = step
        
    def set_model(self, model):
        self.generator = model
        self.model = model
    
    @staticmethod
    def plot_fun(xlr, xhr, ygen, hypar=None, **kwargs):
        from matplotlib import pyplot as plt
        import numpy as np
        from skimage import transform
        from skimage.metrics import structural_similarity

        fig, ax = plt.subplots(3,5, figsize=(20,12))
        true = xhr.numpy()
        fake = ygen
        for i in range(0,3):
            lr_img = xlr[i,...,0]
            hr_img = true[i,...,0]
            gen_img = fake[i,...,0]
            upscaled_lr = transform.resize(lr_img, hr_img.shape, order=1)

            ax0 = ax[i,0].imshow(hr_img, vmin=0, vmax=1)
            plt.colorbar(ax0, ax=ax[i,0])
            ax[i,0].set_title("HR")

            ax1 = ax[i,1].imshow(upscaled_lr, vmin=0, vmax=1)
            plt.colorbar(ax1, ax=ax[i,1])
            upscaled_ssim = structural_similarity(hr_img, upscaled_lr)
            ax[i,1].set_title(f"Upscaled LR\nSSIM: {upscaled_ssim:.3f}")

            ax2 = ax[i,2].imshow(gen_img, vmin=0, vmax=1)
            plt.colorbar(ax2, ax=ax[i,2])
            fake_ssim = structural_similarity(hr_img, gen_img)
            ax[i,2].set_title(f"Fake HR\nSSIM: {fake_ssim:.3f}")

            upscaled_error = np.abs(upscaled_lr - hr_img)

            ax3 = ax[i,3].imshow(upscaled_error)
            plt.colorbar(ax3, ax=ax[i,3])
            ax[i,3].set_title("Upscaled vs HR abs error")

            fake_error = np.abs(upscaled_lr - hr_img)

            ax4 = ax[i,4].imshow(fake_error)
            plt.colorbar(ax4, ax=ax[i,4])
            ax[i,4].set_title("Upscaled vs HR abs error")
        
    def on_train_end(self, logs=None):
        import singan_training as sgt
        im = sgt.plot_samples(self.generator, self.val_batch, hypar=self.hypar, 
                              close=False, return_for_tf=True, plotting_func=self.plot_fun, **self.kwargs)
        tf.summary.image(self.imagelabel, im, step=self.step)
        import matplotlib.pyplot as plt
        plt.close()

            
class MyMetricsLoggerCallback(tf.keras.callbacks.Callback):
    def __init__(self, time_measurement_interval=10, prepend_tag=""):
        self.time_measurement_interval = time_measurement_interval
        
        self._t0 = time.time()
        self.prepend_tag = prepend_tag
    
    def on_train_batch_begin(self, batch, logs=None):
        if self.time_measurement_interval > 0:
            if batch % self.time_measurement_interval == 0:
                self._t0 = time.time()

    def on_train_batch_end(self, batch, logs=None):
        for key in logs:
            tf.summary.scalar(self.prepend_tag+key, logs[key], step=batch)

        if self.time_measurement_interval > 0:
            if batch % self.time_measurement_interval == self.time_measurement_interval - 1:
                tf.summary.scalar(self.prepend_tag+"time_per_step", (time.time() - self._t0)/self.time_measurement_interval, step=batch)
    
    def on_test_batch_end(self, batch, logs=None):
        for key in logs:
            tf.summary.scalar(self.prepend_tag+key, logs[key], step=batch)
            
class PlotOnValidationCallback(tf.keras.callbacks.Callback):
    def __init__(self, plotting_func=None, val_batch=None, imagelabel="test_images", hypar=None, **kwargs):
        self.plotting_func = plotting_func
        self.val_batch = val_batch
        self.hypar = hypar
        self.imagelabel = imagelabel

        self.kwargs = kwargs
        
    def set_model(self, model):
        self.generator = model
        self.model = model

    def on_test_batch_begin(self, batch, logs=None):

        import singan_training as sgt
        im = sgt.plot_samples(self.generator, self.val_batch, hypar=self.hypar, 
                              close=False, return_for_tf=True, plotting_func=self.plotting_func, **self.kwargs)
        tf.summary.image(self.imagelabel, im, step=batch)
        import matplotlib.pyplot as plt
        plt.close()



class OurCallbackList(tf.keras.callbacks.Callback):
    def __init__(self, callbacks=None, model=None, **params):
        self.callbacks = callbacks
        
        if model is not None:
            self.set_model(model)
        if params:
            self.set_params(params)
    
    def append(self, callback):
        self.callbacks.append(callback)

    def set_params(self, params):
        self.params = params
        for callback in self.callbacks:
            callback.set_params(params)

    def set_model(self, model):
        self.model = model
        for callback in self.callbacks:
            callback.set_model(model)
        
    def on_train_begin(self, logs=None):
        for callback in self.callbacks:
            callback.on_train_begin(logs)

    def on_train_end(self, logs=None):
        for callback in self.callbacks:
            callback.on_train_end(logs)

    def on_epoch_begin(self, epoch, logs=None):
        for callback in self.callbacks:
            callback.on_epoch_begin(epoch, logs)

    def on_epoch_end(self, epoch, logs=None):
        for callback in self.callbacks:
            callback.on_epoch_end(epoch, logs)

    def on_test_begin(self, logs=None):
        for callback in self.callbacks:
            callback.on_test_begin( logs)

    def on_test_end(self, logs=None):
        for callback in self.callbacks:
            callback.on_test_end( logs)

    def on_predict_begin(self, logs=None):
        for callback in self.callbacks:
            callback.on_predict_begin(logs)

    def on_predict_end(self, logs=None):
        for callback in self.callbacks:
            callback.on_predict_end( logs)

    def on_train_batch_begin(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_train_batch_begin(batch, logs)

    def on_train_batch_end(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_train_batch_end(batch, logs)

    def on_test_batch_begin(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_test_batch_begin(batch, logs)

    def on_test_batch_end(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_test_batch_end(batch, logs)

    def on_predict_batch_begin(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_predict_batch_begin(batch, logs)

    def on_predict_batch_end(self, batch, logs=None):
        for callback in self.callbacks:
            callback.on_predict_batch_end(batch, logs)
            
class TestCallback(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs=None):
        keys = list(logs.keys())
        print("Starting training; got log keys: {}".format(keys))

    def on_train_end(self, logs=None):
        keys = list(logs.keys())
        print("Stop training; got log keys: {}".format(keys))

    def on_epoch_begin(self, epoch, logs=None):
        keys = list(logs.keys())
        print("Start epoch {} of training; got log keys: {}".format(epoch, keys))

    def on_epoch_end(self, epoch, logs=None):
        keys = list(logs.keys())
        print("End epoch {} of training; got log keys: {}".format(epoch, keys))

    def on_test_begin(self, logs=None):
        keys = list(logs.keys())
        print("Start testing; got log keys: {}".format(keys))

    def on_test_end(self, logs=None):
        keys = list(logs.keys())
        print("Stop testing; got log keys: {}".format(keys))

    def on_predict_begin(self, logs=None):
        keys = list(logs.keys())
        print("Start predicting; got log keys: {}".format(keys))

    def on_predict_end(self, logs=None):
        keys = list(logs.keys())
        print("Stop predicting; got log keys: {}".format(keys))

    def on_train_batch_begin(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Training: start of batch {}; got log keys: {}".format(batch, keys))

    def on_train_batch_end(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Training: end of batch {}; got log keys: {}".format(batch, keys))

    def on_test_batch_begin(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Evaluating: start of batch {}; got log keys: {}".format(batch, keys))

    def on_test_batch_end(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Evaluating: end of batch {}; got log keys: {}".format(batch, keys))

    def on_predict_batch_begin(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Predicting: start of batch {}; got log keys: {}".format(batch, keys))

    def on_predict_batch_end(self, batch, logs=None):
        keys = list(logs.keys())
        print("...Predicting: end of batch {}; got log keys: {}".format(batch, keys))



###### Outdated Code Below !! #######

class SaveModelAtEpochEndCallback(tf.keras.callbacks.Callback):
    def __init__(self, modelname="mymodel", savesdir="saves", verbose=True, save_format="tf"):
        self._modelname = modelname
        self._savesdir = savesdir
        self._verbose = verbose
        self._save_format = save_format
        
        import os
        modeldir =  "%s/%s" % (savesdir, modelname)
        if not os.path.exists(modeldir):
            if verbose:
                print("Creating directory %s" % modeldir)
            os.makedirs(modeldir)
            
    def set_model(self, model):
        self.model = model
        
    def get_filename(self, epoch):
        if self._save_format == "h5":
            filename = "%s/%s/model_epoch_%03d.hdf5" % (self._savesdir, self._modelname, epoch)
        else:
            filename = "%s/%s/model_epoch_%03d" % (self._savesdir, self._modelname, epoch)
        return filename
        
    def on_epoch_end(self, epoch, logs=None):
        filename = self.get_filename(epoch)

        if self._verbose:
            print("\nEpoch %d ended, saving model to %s" % (epoch, filename))
        
        tf.keras.models.save_model(self.model, filename, save_format=self._save_format)
        
    def load_model_of_epoch(self, epoch):
        filename = self.get_filename(epoch)
        
        if self._verbose:
            print("Loading epoch %d modelfile %s" % (epoch, filename))
        return tf.keras.models.load_model(filename)
    
from tensorboard.plugins.hparams import api as hp
class HyperParamCallback(tf.keras.callbacks.Callback):
    def __init__(self, logdir="logs/hyperparam", testdata=None, hparams=None, perepoch=False):
        
        self._x = 3
        self._testdata = testdata
        self._model = None
        
        self._logdir = logdir
        
        self._writer = None
        
        self._model = None
        
        self._hparamdef=None
        
        self._perepoch = perepoch
        
    def set_hparam(self, label=None, **kwargs):
        # the input hparam is simply a dict of name:value
        if self._hparamdef is None:
            self._hparamdef = {key:hp.HParam(key) for key in kwargs.keys()}
        else:
            for key in kwargs:
                assert key in self._hparamdef.keys(), "Missing previously used parameter %s" % key
        
        self._hparams = {self._hparamdef[key]:kwargs[key] for key in kwargs.keys()}

        self._label = label
        
        
    def set_model(self, model):
        assert self._hparamdef is not None, "call set_hparam first please"
        
        if self._model is None: # first call -> set up hyper parameter's metrics
            with tf.summary.create_file_writer(self._logdir).as_default():
                metrics = [hp.Metric(metric_name, display_name=metric_name) for metric_name in model.metrics_names]
                
                hp.hparams_config(hparams=list(self._hparamdef.values()), metrics=metrics)       
                
        self._model = model
        
        self._metrics_names = model.metrics_names
        
        if self._writer is not None:
            del self._writer
            
        model_logdir = self._logdir
        if self._label is not None:
            model_logdir = model_logdir + "/" + self._label
        self._writer = tf.summary.create_file_writer(model_logdir)
        
    def on_train_begin(self, logs=None):
        with self._writer.as_default():
            hp.hparams(self._hparams)
        
    def on_epoch_end(self, epoch, logs=None):
        if self._perepoch:
            metrics = self._model.evaluate(self._testdata[0], self._testdata[1], verbose=0)
            with self._writer.as_default():
                for metric, metric_name in zip(metrics, self._metrics_names):
                    tf.summary.scalar(metric_name, metric, step=epoch)

        
    def on_train_end(self, logs=None):
        if not self._perepoch:
            metrics = self._model.evaluate(self._testdata[0], self._testdata[1], verbose=0)
            with self._writer.as_default():
                for metric, metric_name in zip(metrics, self._metrics_names):
                    tf.summary.scalar(metric_name, metric, step=self._last_epoch+1)
        
