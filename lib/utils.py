import numpy as np

def copy_to_dir(src, destdir):
    import glob
    import shutil
    import os
    
    os.makedirs(destdir, exist_ok=True)
    for file in glob.glob(src):
        shutil.copy(file, destdir)


def uniform_grid_nd(npix, L=1., endpoint=False):
    assert len(np.shape(npix)) > 0, "Please give npix in form (npixx,npixy,..)"
    
    L = np.ones_like(npix) * L
    
    ardim = [np.linspace(0, Li, npixi, endpoint=endpoint) for npixi, Li in zip(npix, L)]
    
    q = np.stack(np.meshgrid(*ardim, indexing="ij"), axis=-1)
    
    return q

def get_random_slices(pos, size=10, seed=None, ngrid=512, L=20.):
    if seed is not None:
        np.random.seed(seed)
    myslices = np.random.randint(0, ngrid, size=size)
    
    ax = np.random.randint(0, 3, size=size)
    
    posout = np.zeros((size,ngrid,ngrid,2))
    
    posout[ax==0] = pos[myslices[ax==0], ..., 1:3]
    posout[ax==1] = np.rollaxis(pos[:,myslices[ax==1]][...,(0,2)], 1, 0)
    posout[ax==2] = np.rollaxis(pos[:,:,myslices[ax==2], 0:2], 2, 0)
    
    for ax in (0,1):
        di = np.random.randint(0, pos.shape[-3+ax])
        posout = np.roll(posout, di, axis=-3+ax)
        
    return posout

def subsamp_fields(s, subsamp):
    ims = []
    for ix in range(0, subsamp):
        for iy in range(0, subsamp):
            ims.append(s[ix::subsamp, iy::subsamp])
    return ims

def subsamp_3Dfields(s, subsamp):
    ims = []
    for ix in range(0, subsamp):
        for iy in range(0, subsamp):
            for iz in range(0, subsamp):
                ims.append(s[ix::subsamp, iy::subsamp, iz::subsamp])
    return ims


def upscale_fields(s, upsamp):
    if s is None:
        return None
    
    imin = s
    imout = np.zeros(imin.shape[:-3] + (imin.shape[-3]*upsamp, imin.shape[-2]*upsamp, imin.shape[-1]), dtype=np.float32)
    
    for ix in range(0, upsamp):
        for iy in range(0, upsamp):
            imout[...,ix::upsamp,iy::upsamp,:] = imin
    return imout

def split_fields(s, chunk=(3*32,3*32), flat=True, fileformat="npy"):
    ax = 0
    
    nx = s.shape[-3] // chunk[0]
    ny = s.shape[-2] // chunk[1]
    
    ssel = s[...,:nx*chunk[0], :ny*chunk[1], :]
    
    snew = np.stack(np.split(ssel, nx, axis=-3), axis=-4)
    snew = np.stack(np.split(snew, ny, axis=-2), axis=-4)
    
    if flat:
        if fileformat=="npy":
            return snew.reshape((-1, chunk[0], chunk[1], 2))
        elif fileformat=="png":
            return snew.reshape((-1, chunk[0], chunk[1], 1))
    else:
        return snew

def split_3Dfields(ss, chunk=(3*32,3*32,3*32), flat=True):
    
    nx = ss.shape[-4] // chunk[0]
    ny = ss.shape[-3] // chunk[1]
    nz = ss.shape[-2] // chunk[2]
    
    ssel = ss[...,:nx*chunk[0], :ny*chunk[1], :nz*chunk[2],:]
    
    snew = np.stack(np.split(ssel, nx, axis=-4), axis=-5)
    snew = np.stack(np.split(snew, ny, axis=-3), axis=-5)
    snew = np.stack(np.split(snew, nz, axis=-2), axis=-5)
    
    if flat:
        return snew.reshape((-1, chunk[0], chunk[1], chunk[2], 3))
    else:
        return snew
    
def periodic_padding_ax(v, axis, npad=2):
    npads = [(0,0)] * len(v.shape)
    for ax in np.atleast_1d(axis):
        npads[ax] = (npad,npad)
        
    return np.pad(v, npads, "wrap")

def remove_wrapping(xin, L, zerowrap=True):
    """Makes sure there exist no wrappings in x,
       except at the domain boundaries"""
    x = np.copy(xin)
    
    ndim = x.shape[-1]
    field_dim = tuple(range(len(x.shape) - ndim - 1, len(x.shape)-1))
    
    for ax in field_dim:
        xp = np.roll(x, 1, ax)

        xw = np.cumsum(np.round((xp-x) / L), axis=ax)

        x = x + xw*L

    return x

def resample_2d(x, nsamp, qperiodic=True, pos=False, L=None, order=3):
    ndim = 2
    
    if qperiodic:
        npad = 2
        x = periodic_padding_ax(x, (-3,-2), npad=2)
    if pos:
        x = remove_wrapping(x % L, L)
        
    qinx = np.arange(0, x.shape[0], dtype=np.float64)
    qiny = np.arange(0, x.shape[1], dtype=np.float64)
    
    if qperiodic:
        qoutx = np.linspace(2, x.shape[0]-2, nsamp, endpoint=False)
        qouty = np.linspace(2, x.shape[1]-2, nsamp, endpoint=False)
    else:
        qoutx = np.linspace(0, x.shape[0]-1, nsamp)
        qouty = np.linspace(0, x.shape[1]-1, nsamp)
    
    xout = np.zeros(qoutx.shape + qouty.shape + (x.shape[-1],))
    
    for ax in range(0, x.shape[-1]):
        import scipy.interpolate as ip
        xinterp    = ip.RectBivariateSpline(qinx, qiny, x[...,ax], kx=order, ky=order)
        xout[...,ax] = xinterp(qoutx, qouty)
    
    if(pos):
        xout = xout % L        
        
    return xout

def remove_center(s, setto=0.):
    assert (s.shape[-3] % 3 == 0) & (s.shape[-2] % 3 == 0), s.shape
    
    noff = np.array(s.shape[-3:-1])//3

    s = np.copy(s)
    s[...,noff[0]:2*noff[0], noff[1]:2*noff[1],:] = setto
    
    return s

def select_center(s):
    assert (s.shape[-3] % 3 == 0) & (s.shape[-2] % 3 == 0), s.shape
    
    noff = np.array(s.shape[-3:-1])//3

    return s[...,noff[0]:2*noff[0], noff[1]:2*noff[1],:]

def combine_center(s, scent):
    assert (s.shape[-3] % 3 == 0) & (s.shape[-2] % 3 == 0), s.shape
    
    noff = np.array(s.shape[-3:-1])//3

    snew = np.copy(s)
    snew[...,noff[0]:2*noff[0], noff[1]:2*noff[1],:] = scent
    return snew

def disp_to_density(s, nsamp=512*3, qext=20., qperiodic=False, 
                    x0=None, dx=None, nbins=256, removecenter=False):
    #qext = np.array(s.shape[-3:-1]) * L / npartbox
    q = uniform_grid_nd(s.shape[-3:-1], L = qext)
    
    x = s + uniform_grid_nd(s.shape[-3:-1], L = qext)
    
    if x0 is None:
        x0 = np.mean(x, axis=(-2,-3))
    x -= x0
    
    if dx is None:
        dx = np.max(np.abs(x), axis=(-2,-3))
    
    if nsamp > 0:
        xres = resample_2d(x, nsamp=nsamp, order=3, qperiodic=qperiodic)
    else:
        xres = x
    
    if removecenter:
        assert nsamp % 3 == 0, nsamp
        
        xres = remove_center(xres, setto=np.max(dx)*2.)
    
    bins = [np.linspace(-dxi,dxi, num=nbins) for dxi in dx]
    
    rho,_ = np.histogramdd(xres.reshape(-1,2), bins=bins)
    
    return rho, [-dx[0], dx[0], -dx[1], dx[1]]

def my_distortion_tensor_fd(s, qext, npartbox=512, dim=3):
    # we work with the displacement field, since it is periodic
    dsq = np.zeros(s.shape + (dim,))
    
    dim = s.shape[-1]
    eij = np.diag(np.ones(dim))
    
    #qext = s.shape[0] * L / npartbox
    
    hi = qext / s.shape[0]
    
    for ax in range(0, dim):
        sp = np.roll(s, -1, axis=ax)
        sm = np.roll(s, 1, axis=ax)
        
        dsq[...,ax,:] = (sp - sm) / (2.*hi)

    return dsq + np.diag((1.,)*dim )

def plot_slice_stats(ax, disp, qext, removecenter=False, nsamp=512*3, nbins=256):
    import matplotlib.pyplot as plt
    
    rho, dx = disp_to_density(disp, qext=qext, removecenter=removecenter, nsamp=nsamp, nbins=nbins)
    
    if removecenter:
        disp = remove_center(disp)

    s = np.sqrt(np.sum(disp**2, axis=-1))
    dxq = my_distortion_tensor_fd(disp, qext=qext, dim=2)
    
    ax[0].imshow(s, interpolation="nearest", cmap="afmhot")
    ax[1].imshow(-np.log10(np.abs(np.linalg.det(dxq))), vmin=-2, vmax=2, cmap="seismic", interpolation="nearest")
    ax[2].imshow(np.log10(rho+0.1), interpolation="nearest")
    
    for col in range(0, 3):
        ax[col].set_xticks([])
        ax[col].set_yticks([])
        
def plot_slice_stats_new(ax, disp, qext, removecenter=False, labeled=False, nsamp=512*3, nbins=256):
    import matplotlib.pyplot as plt
    
    rho, dx = disp_to_density(disp, qext=qext, removecenter=removecenter, nsamp=nsamp, nbins=nbins)
    
    if removecenter:
        disp = remove_center(disp)

    s = np.sqrt(np.sum(disp**2, axis=-1))
    dxq = my_distortion_tensor_fd(disp, qext=qext, dim=2)
    
    ax[0].imshow(disp[...,0], interpolation="nearest", cmap="afmhot")
    ax[1].imshow(disp[...,1], interpolation="nearest", cmap="afmhot")
    ax[2].imshow(-np.log10(np.abs(np.linalg.det(dxq))), vmin=-2, vmax=2, cmap="seismic", interpolation="nearest")
    ax[3].imshow(np.log10(rho+0.1), interpolation="nearest")
    
    for col in range(0, 4):
        ax[col].set_xticks([])
        ax[col].set_yticks([])
        
    if labeled:
        ax[0].set_title(r"$s_x (q)$", fontsize=14)
        ax[1].set_title(r"$s_y (q)$", fontsize=14)
        ax[2].set_title(r"$\rho_s (q)$", fontsize=14)
        ax[3].set_title(r"$\rho (x)$", fontsize=14)
        
        
def zeldovichify_image(im, smoothscale=10., subsamp=None, norm=1., targetwidth=None, transpose=False):
    import scipy.ndimage as ndi
    
    if len(im.shape) >= 3: # sum over channels
        assert (len(im.shape) == 3)
        phi = np.sqrt(np.sum(im**2, axis=-1))
    else:
        assert len(im.shape) == 2
        phi = im
        
    if transpose:
        phi = phi.T
    
    if smoothscale > 0.:
        phi = ndi.gaussian_filter(phi, sigma=smoothscale)
        
    if targetwidth is not None:
        subsamp = phi.shape[1] // targetwidth
        phi = phi[::subsamp,::subsamp] #, s[::subsamp,::subsamp]
        
    # get gradient = displacement field
    dx = 1./np.array(phi.shape[0])
    s = -np.stack([np.roll(phi, 1, axis=ax) - np.roll(phi, -1, axis=ax) for ax in (0,1)], axis=-1) / dx
    
    if norm is not None:
        phi *= norm / np.std(s)
        s *= norm / np.std(s)
    
    return phi, s