import tensorflow as tf
from tensorflow.keras import layers

class MySequentialConvNet(tf.keras.Model):
    def __init__(self, convlayers=(32,32), denslayers=(128,), dropoutconv=0.1, dropoutdense=0.2, nout=10, name=None):
        def list_to_str(x=(1,2)):
            mystr = str(x[0])
            for xi in x[1:]:
                mystr += "_" + str(xi)
            return mystr
        
        if name is None:
            name = "conv_" + list_to_str(convlayers) + "_dens_" + list_to_str(denslayers) + "_d1_" + str(dropoutconv) + "_d2_" + str(dropoutdense)
        
        super(MySequentialConvNet, self).__init__(name=name)
        
        self.seqlayers = []
        
        for conv in convlayers:
            self.seqlayers.append(layers.Conv2D(conv, (3, 3), activation='relu', padding="same"))
            self.seqlayers.append(layers.Dropout(dropoutconv))
            self.seqlayers.append(layers.MaxPooling2D((2, 2)))
            
        self.seqlayers.append(layers.Flatten())
        
        for nneurons in denslayers:
            self.seqlayers.append(layers.Dense(nneurons, activation='relu'))
            self.seqlayers.append(layers.Dropout(dropoutdense))
        
        # Output layer
        self.seqlayers.append(layers.Dense(nout, activation='softmax'))

    def call(self, inputs, training=False):
        x = inputs
        for lay in self.seqlayers:
            if isinstance(lay,layers.Dropout):
                x = lay(x, training=training)
            else:
                x = lay(x)
                
        return x

def list_to_str(x=(1,2)):
    if len(x) == 0:
        return ""
    mystr = str(x[0])
    for xi in x[1:]:
        mystr += "_" + str(xi)
    return mystr

def sequential_conv_net(convnum=1, convlayers=(32,32), denslayers=(128,), dropoutconv=0.1, dropoutdense=0.2, inputshape=(32,32,3), nout=10, name=None):
    inputs = tf.keras.Input(shape=inputshape)
    x = inputs
    for conv in convlayers:
        for i in range(0, convnum):
            x = layers.Conv2D(conv, (3, 3), activation='relu', padding="same")(x)
        x = layers.Dropout(dropoutconv)(x)
        x = layers.MaxPooling2D((2, 2))(x)
    x = layers.Flatten()(x)
    
    for nneurons in denslayers:
        x = layers.Dense(nneurons, activation='relu')(x)
        x = layers.Dropout(dropoutdense)(x)
        
    outputs = layers.Dense(nout, activation='softmax')(x)
        
    if name is None:
        name = "conv_" + list_to_str(convlayers) + "_dens_" + list_to_str(denslayers) + "_d1_" + str(dropoutconv) + "_d2_" + str(dropoutdense)
        
    return tf.keras.Model(inputs=inputs, outputs=outputs, name=name)

def layer_increase_channels(x, kernel_size=(3,3), filters=16):
    return layers.Conv2D(filters, kernel_size, padding="same")(x)

def res_block(x, filters=(16,16), strides=1):
    y = layers.Conv2D(filters[0], (3, 3), strides=strides, padding="same")(x)
    y = layers.BatchNormalization(axis=-1)(y)
    y = layers.Activation("relu")(y)
    
    y = layers.Conv2D(filters[1], (3, 3), padding="same")(y)
    y = layers.BatchNormalization(axis=-1)(y)
    y = layers.Activation("relu")(y)
    
    if strides > 1:
        x = layers.Conv2D(filters[1], (1, 1), strides=strides, padding="same")(x)
    else:
        assert filters[1] == x.shape[-1], "wrong filters dimension %s %s" % (str(filters), str(x.shape))
        
    x = layers.Add()([x, y])
    x = layers.Activation("relu")(x)
    
    return x

def res_net_20_v1(inputshape, outputshape, filters=16, name="resnet20"):
    inputs = tf.keras.Input(shape=inputshape)
    x = layer_increase_channels(inputs, filters=filters)
    
    x = res_block(x, filters=(filters,filters))
    x = res_block(x, filters=(filters,filters))
    x = res_block(x, filters=(filters,filters))
    
    x = res_block(x, filters=(filters*2,filters*2), strides=2)
    x = res_block(x, filters=(filters*2,filters*2))
    x = res_block(x, filters=(filters*2,filters*2))
    
    x = res_block(x, filters=(filters*4,filters*4), strides=2)
    x = res_block(x, filters=(filters*4,filters*4))
    x = res_block(x, filters=(filters*4,filters*4))
    
    x = layers.AveragePooling2D(pool_size=8)(x)
    x = layers.Flatten()(x)
    
    outputs = layers.Dense(outputshape, activation='softmax')(x)
    
    return tf.keras.Model(inputs=inputs, outputs=outputs, name=name)

def unet_v1(inputshape, outchannels=1, name="unet_v1"):
    inputs = tf.keras.Input(shape=inputshape)
    
    def conv_block(inputs, nchan, pool=True, upconv=False, concat=None):
        x = inputs
        
        if pool:
            x = layers.MaxPooling2D((2,2))(x)
        if upconv:
            x = layers.Conv2DTranspose(nchan, (2, 2), strides=2, padding="same", activation="relu")(x)
            
            assert not pool
            
        if concat is not None:
            x = layers.concatenate([x,concat], axis=-1)
        
        x = layers.Conv2D(nchan, (3, 3), padding="same", activation="relu")(x)
        x = layers.Conv2D(nchan, (3, 3), padding="same", activation="relu")(x)
        x = layers.Conv2D(nchan, (3, 3), padding="same", activation="relu")(x)
        
        return x
    
    def upsamp_block(inputs, nchan, pool=True, upsamp=2):
        x = layers.Conv2DTranspose(nchan, (3, 3), strides=upsamp, padding="same", activation="relu")(inputs)
        x = layers.Conv2DTranspose(nchan, (3, 3), strides=upsamp, padding="same", activation="relu")
        x = layers.Conv2DTranspose(nchan, (3, 3), strides=upsamp, padding="same", activation="relu")
    
    x0 = conv_block(inputs, 42, pool=False)
    x1 = conv_block(x0, 42)
    x2 = conv_block(x1, 42)
    x3 = conv_block(x2, 42)
    x4 = conv_block(x3, 42)
    
    u3 = conv_block(x4, 42, pool=False, upconv=True, concat=x3)
    u2 = conv_block(u3, 42, pool=False, upconv=True, concat=x2)
    u1 = conv_block(u2, 42, pool=False, upconv=True, concat=x1)
    u0 = conv_block(u1, 42, pool=False, upconv=True, concat=x0)
    
    outputs = layers.Conv2D(outchannels, (1, 1), padding="same", activation="sigmoid")(u0)
    
    return tf.keras.Model(inputs=inputs, outputs=outputs, name=name)