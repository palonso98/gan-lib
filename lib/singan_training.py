import tensorflow as tf
import tensorflow.keras.layers as layers
import numpy as np
import scipy as scp
import singan as sg
import time
import utils as ut
import callbacks as cb
from tensorboard.plugins.hparams import api as hp
import io
import os
import datasets as dt
try:
    import bacco.visualization as vis
except:
    print("Warning, couldn't load bacco, running without")
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from skimage.metrics import structural_similarity

#from memory_profiler import profile


# The following dictionary defines "superhyperparameters" which select groups of detailed hyperparameters
# add your combination of hyper parameters here if have a good combination you want to come back to later
hpg = dict(architecture={}, network={}, training={}, dataset={})

base_wasgp = dict(gradpencoeff=10., dchan=32, nconvlayers=5, batchnorm="layer", gradient=1., gradfactor=1.)
hpg["architecture"]["singanpaper"] = {**base_wasgp, **dict(addlr=True, addnoise=True, reclosscoeff=10., reclosstype="l2zeronoise", genactivation="tanh")}
base_l1rec = dict(reclosscoeff=10., reclosstype="l1")
hpg["architecture"]["ganlag2d"] = {**base_wasgp, **base_l1rec, **dict(subtractmean=True)}
hpg["architecture"]["gandmsuperres"] = {**base_wasgp, **base_l1rec, **dict(mask="001,011,101")}
#hpg["architecture"]["gandmcondsuperres"] = {**base_wasgp, **base_l1rec, **dict(batchnorm="layer", mask="011,101")}
#hpg["architecture"]["ganhalotodm"] = {**base_wasgp, **base_l1rec, **dict(batchnorm="layer", mask="000,011,101")}


hpg["training"]["default"] = dict(critic_iter=5, lrdecrease=100000, batchsize=16, adam=True, learningrate=5e-4)

hpg["dataset"]["lag2d"] = dict(spatchsize=64, takelog=False, avg=0, ndim=2)
hpg["dataset"]["lag2dinterp"] = dict(patchsize=64, takelog=False, avg=0, ndim=2)
hpg["dataset"]["dmhalo200"] = dict(patchsize=64, takelog=True, avg=1, ndim=2)
hpg["dataset"]["dmhalo800"] = dict(patchsize=64, takelog=True, avg=1, ndim=2)
hpg["dataset"]["microscopy"] = dict(num_x_patches=4, num_y_patches=4, takelog=False, avg=2, ndim=2, nchan=1, mask="")

hpg["dataset"]["lag3d_planck1536"] = dict(patchsize=64,  takelog=False, avg=0, ndim=3, snapind=94, subsamp=8,
                                          simdir='/scratch-EDR/cosmosims/N1536_L512/Planck18_N1536_L512/3.14/')

# This dictionary defines default hyper parameters, which one usually does not bother with.
# For example, if you add a new experimental hyper-parameter, then add here the value which
# is needed to keep the new feature turned off
hp_misc_defaults = dict(genactivation="linear", stabloss=0., input_lr_too=False, fakemodel="None", lr_dm_only="auto", npcoeff=0., npstddev=0., mask="", subtractmean=False, spectralnorm=False, addlr=False, addnoise=False)

def assemble_hypar(hp, hpgroups, misc_defaults, verbose=1):
    allhp = {}
    
    for grouptype in hpgroups:
        if grouptype in hp.keys():
            groupname = hp[grouptype]
            
            if verbose >= 2:
                print("Using hypars from group %s->%s" % (grouptype, groupname))
                
            assert groupname in hpgroups[grouptype], "Could not find group %s->%s in %s->%s" % (grouptype, groupname,  grouptype, str(hpgroups[grouptype].keys()))
            
            addhps = hpgroups[grouptype][groupname]
            
            for key in addhps:
                if key in allhp:
                    if (verbose >=1) & (addhps[key] != allhp[key]):
                        print("Warning: Overwriting parameter %s=%s given from another group through %s->%s->%s=%s" % (key, allhp[key], grouptype,groupname,key,addhps[key]))
                    allhp[key] = addhps[key]
                else:
                    allhp[key] = addhps[key]
                    
    for key in hp:
        if (key in allhp) & (verbose >= 2):
            print("Overwriting parameter %s=%s given from a group through %s=%s" % (key, allhp[key], key, hp[key]))
        allhp[key] = hp[key]
        
    for key in misc_defaults:
        if not key in allhp:
            allhp[key] = misc_defaults[key]
                    
    return allhp
                
class hp_manager(dict):
    def __init__(self, hpgroups=None, verbose=1, **kwargs):
        if hpgroups is None:
            self.hpgroups = hpg.copy()
        else:
            self.hpgroups = hpgroups
            
        assert "architecture" in kwargs.keys(), "Please provide 'architecture' hyper parameter"
        assert "dataset" in kwargs.keys(), "Please provide 'dataset' hyper parameter"
        
        if not "training" in kwargs.keys():
            if verbose >= 1:
                print("Warning: adding training='default' to the hyper parameters")
            kwargs["training"] = "default"
        
            
        fullhp = assemble_hypar(kwargs, self.hpgroups, hp_misc_defaults, verbose=verbose) 
        
        self.inhp = kwargs
        
        dict.__init__(self, **fullhp)
        
    def to_name(self):
        mystr = None
        for key in self.inhp:
            if mystr is None:
                mystr = "%s=%s" % (str(key), str(self.inhp[key]))
            else:
                mystr += "_%s=%s" % (str(key), str(self.inhp[key]))
        return mystr
        
    def __setitem__(self, key, val):
        self.inhp[key] = val
        
        if key in self.hpgroups: # have to reassamble then
            fullhp = assemble_hypar(self.inhp, self.hpgroups, hp_misc_defaults) 
            dict.update(self, **fullhp)
        else:
            dict.__setitem__(self, key, val)
            
def limit_memory(mbytes):
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=mbytes)])
        except RuntimeError as e:
            print(e)
        

def define_models(hypar, generator_fun=sg.define_singan_generator, critic_fun=sg.define_singan_critic):
    
    assert hypar["mask"].find("110")<0
    assert hypar["mask"].find("111")<0
    
    inputmask_gen = []
    if hypar["mask"].find("000")>-1:
        inputmask_gen.append(0)
    if hypar["mask"].find("001")>-1:
        inputmask_gen.append(1)
        
    if len(inputmask_gen) == 0:
        inputmask_gen = []
        
    if hypar["mask"].find("010")>-1:
        outputmask_gen = (0,)
    elif hypar["mask"].find("011")>-1:
        outputmask_gen = (1,)
    else:
        outputmask_gen = []
        
    if hypar["mask"].find("100")>-1:
        inputmask_cr = (0,)
    elif hypar["mask"].find("101")>-1:
        inputmask_cr = (1,)
    else:
        inputmask_cr = []
        
    if "ndim" in hypar:
        ndim = hypar["ndim"]
        nchan = ndim
    else:
        ndim = 2
        nchan = 2
        
    if "nchan" in hypar:
        nchan = hypar["nchan"]
    
    generator = generator_fun(
        ndim = ndim,
        nchan=nchan, d=hypar["dchan"], spectral_norm=hypar["spectralnorm"], 
        batch_norm=hypar["batchnorm"], addnoise=hypar["addnoise"], 
        nconvlayers=hypar["nconvlayers"],takelog=hypar["takelog"],
        outputmask=outputmask_gen,inputmask=inputmask_gen,
        activation=hypar["genactivation"], addlr=hypar["addlr"],
        avg=hypar["avg"], lr_dm_only=hypar["lr_dm_only"],
        sigma=hypar["sigma"]
    )

    generator.summary()
    
    critic = critic_fun(
        ndim = ndim,
        nchan=nchan, d=hypar["dchan"], spectral_norm=hypar["spectralnorm"], 
        batch_norm=hypar["batchnorm"], input_lr_too=hypar["input_lr_too"], 
        takelog=hypar["takelog"], subtractmean=hypar["subtractmean"], inputmask=inputmask_cr
    )

    critic.summary()

    if hypar["adam"]:
        opt = tf.keras.optimizers.Adam(learning_rate=hypar["learningrate"], beta_1=0.5, beta_2=0.9) # 1e-4 default here
    else:
        opt = tf.keras.optimizers.RMSprop(learning_rate=hypar["learningrate"])
    
    critic.trainable = True
    wasserstein = sg.SinGanWassersteinModel(critic)
    wasserstein.compile(optimizer=opt, loss=wasserstein.get_loss_functions(lam=hypar["gradpencoeff"], stabilization_loss=hypar["stabloss"], gradfactor=hypar["gradfactor"], gradient=hypar["gradient"]))

    #wasserstein.summary()
    
    critic.trainable = False
    gan = sg.singan_model(generator, critic, optimizer=opt, recloss_coeff=hypar["reclosscoeff"],
                          recloss_type=hypar["reclosstype"],                         
                          noise_penalty_coeff=hypar["npcoeff"], noise_penalty_stddev=hypar["npstddev"])
    critic.trainable = True

    gan.summary()
    
    return wasserstein, gan, generator, critic

def define_checkpointed_models(hypar, load=True, dirlabel="", for_training=True, basedir=".", clone=None, cpname=None, verbose=1, generator_fun=sg.define_singan_generator, critic_fun=sg.define_singan_critic):
    name = hypar.to_name()
    name = hypar["tag"]
    #name = "test"
    
    wasserstein, gan, generator, critic = define_models(hypar, generator_fun, critic_fun)
    
    mycp = tf.train.Checkpoint(ganoptimizer=gan.optimizer, gan=gan, wsoptimizer=wasserstein.optimizer)
    
    checkpointdir = "%s/checkpoints%s/%s" % (basedir, dirlabel, name)

    if load:
        latest_checkpoint = tf.train.latest_checkpoint(checkpointdir)
        
        if (latest_checkpoint is None) & (clone is not None):

            clonename = clonehypar.to_name()
            clonedir = "%s/checkpoints%s/%s" % (basedir, dirlabel, clonename)
            
            print("Cloning from %s" % (latest_checkpoint,))
            latest_checkpoint = tf.train.latest_checkpoint(clonedir)

        if latest_checkpoint is None:
            if verbose >= 1:
                print("Could not find any checkpoints for model %s in %s" % (name, checkpointdir))
        else:
            if cpname is not None:
                try:
                    status = mycp.restore(checkpointdir+"/"+cpname).expect_partial()
                    if verbose >= 1:
                        print("Restored (iteration = %d) checkpoint %s for model %s" % (gan.iteration, cpname, name))
                except tf.errors.NotFoundError:
                    print("Warning Could not find checkpoint with name %s for model %s -- falling back to latest checkpoint" % (cpname, name))
                    status = mycp.restore(latest_checkpoint).expect_partial()
                    print("Restored latest (iteration=%d) checkpoint for model %s from %s" % (gan.iteration, name, latest_checkpoint))
            else:
                status = mycp.restore(latest_checkpoint).expect_partial()
                if verbose >= 1:
                    print("Restored latest (iteration=%d) checkpoint for model %s from %s" % (gan.iteration, name, latest_checkpoint))
            
    
    return wasserstein, gan, generator, critic, mycp

def plotting_func_lag3d(xlr, xhr, ygen, hypar=None, **kwargs):
    sllr = xlr.shape[-2] // 4
    slhr = xhr.shape[-2] // 4
    
    myxlr, myxhr, myygen = [], [], []
    for i in range(0,4):
        myxlr.append(xlr[0,...,sllr*i,0:2])
        myxhr.append(xhr[0,...,slhr*i,0:2])
        myygen.append(ygen[0,...,slhr*i,0:2])
    myxlr, myxhr, myygen = np.array(myxlr), np.array(myxhr), np.array(myygen)
    
    
    if hypar["dataset"] == "lag3d_planck1536":
        L = 512.
        baseres = 1536 / hypar["subsamp"]
        
        qext = L*myxhr.shape[1]/baseres  
    else:
        raise ValueError("Don't know how to handle this dataset ", hypar["dataset"])
        
        
    plotting_func_lag2d(myxlr, myxhr, myygen, qext=qext, **kwargs)
    

def plotting_func_lag2d(xlr, xhr, ygen, hypar=None, mode="gen", singlemode=False,
                       npartresamp=512*3, nbins=256, transpose=False, savename=None, dpi=120, close=False, qext=None, **kwargs):
    assert len(xlr.shape) <= 4, "ndim=3 not handled here"
    
    
    if qext is None:
        dataset = hypar["dataset"]
        subsamp = hypar["subsamp"]

        if "interp" in dataset:
            baseres = 1024 / subsamp
        else:
            baseres = 512 / subsamp
        
        qext = 20.*xhr.shape[1]/baseres    
    
    if mode == "hr":
        ygc = xhr
        title = "true highres"
    elif mode == "lr":
        ygc = xlr
        title = "input lowres"
    elif mode == "gen":
        ygc = ygen
        title = "generated"
    elif mode == "lrnoise":
        ygc = xlr
        title = "noise"
    else:
        raise ValueError("Unknown mode %s" % mode)

    if transpose:
        ygc = np.swapaxes(ygc, -2,-3)
        ygc[...,:] = ygc[...,::-1]
    
    if singlemode:
        fig, ax = plt.subplots(2, 2, figsize=(11,11))
        ut.plot_slice_stats_new(ax.flat, np.array(ygc[0]), qext, removecenter=False, labeled=True, nsamp=npartresamp, nbins=nbins)
        plt.subplots_adjust(hspace=0.015, wspace=0.015)
    else:
        fig, ax = plt.subplots(4, 4, figsize=(11,11))
        plt.subplots_adjust(hspace=0.015, wspace=0.015)
        for i in range(0,4):
            ut.plot_slice_stats_new(ax[i], np.array(ygc[i]), qext, removecenter=False, labeled=i==0, nsamp=npartresamp, nbins=nbins)
            
    fig.suptitle(title, fontsize=16, y=0.91)
            
    if savename is not None:
        plt.savefig(savename, bbox_inches="tight", dpi=dpi)
    if close:
        plt.close()

def plotting_func_dm_single(xlr,xhr,ygen,hypar=None, savename=None, dpi=120, close=False, pixels=None, mode="gen", **kwargs): 
    from matplotlib.colors import LogNorm
    if pixels is None:
        pixels = ygen.shape[1:3]
    fig, ax = plt.subplots(figsize=(pixels[0]/float(dpi), pixels[1]/float(dpi)), dpi=dpi, frameon=False)
    
    if mode == "hr":
        ygc = xhr
        title = "true highres"
    elif mode == "lr":
        ygc = xlr
        title = "input lowres"
    elif mode == "gen":
        ygc = ygen
        title = "generated"
    elif mode == "lrnoise":
        ygc = xlr
        title = "noise"
    else:
        raise ValueError("Unknown mode %s" % mode)
    
    #print(ygen.shape)
    vmin, vmax = 1e-1, 1e3
    ax.imshow(ygc[0,...,0], cmap="cubehelix", norm=LogNorm(vmin=vmin, vmax=vmax))
    ax.set_xticks([])  # remove xticks
    ax.set_yticks([])  # remove yticks
    ax.axis('off')     # hide axis
    fig.subplots_adjust(bottom=0, top=1, left=0, right=1, wspace=0, hspace=0)  # streches the 
    
    fig.suptitle(title, fontsize=16, y=0.96, color='white')
    
    if savename is not None:
        fig.savefig(savename, dpi=dpi)
    if close:
        plt.close()
    
            
def plotting_func_dm(xlr,xhr,ygen,hypar=None,loglogscale=True, nbins=30, savename=None, dpi=120, close=False, **kwargs): 
    
    assert hypar is not None
    
    takelog = hypar["takelog"]
    dataset = hypar["dataset"]
    
    if "200" in dataset:
        L = 200.
    elif "800" in dataset:
        L = 800.
    else:
        raise ValueError("Problem!")
    
    subsamp = hypar["subsamp"] # can use any way you want
    
    #remember to pass the argument takelog to do_training(...) 
    
    # Target HR IMAGE
    hr_dm = np.array(xhr[0,:,:,0])
    hr_halo = np.array(xhr[0,:,:,1]) 
    
    # Input LR IMAGE
    lr_dm = np.array(xlr[0,:,:,0])
    lr_halo = np.array(xlr[0,:,:,1])
   
    # Output PREDICTED IMAGE
    gen_dm = np.array(ygen[0,:,:,0])
    gen_halo = np.array(ygen[0,:,:,1])
    
    images = np.array([[hr_dm,lr_dm,gen_dm],[hr_halo,lr_halo,gen_halo]])
    
    hr_ims = np.matrix([])
    gen_ims = np.matrix([])
    #THIS HERE IS HARDCODED. UPDATE WHEN POSSIBLE
    hr_ps = np.zeros(1000)
    lr_ps = np.zeros(1000)
    gen_ps = np.zeros(1000)
    
    for k in range(0, xhr.shape[0]):
        hr_ims=np.concatenate((hr_ims,np.log(np.matrix(xhr[k,:,:,0]).flatten())),axis=-1)
        gen_ims=np.concatenate((gen_ims,np.log(np.matrix(ygen[k,:,:,0]).flatten())),axis=-1)
        t, ps = power_spectrum_2d(xhr[k,:,:,0],L=L/512*hr_dm.shape[0])
        hr_ps += ps
        lr_ps += np.array(power_spectrum_2d(xlr[k,:,:,0],L=L/512*lr_dm.shape[0])[1])
        gen_ps += np.array(power_spectrum_2d(ygen[k,:,:,0],L=L/512*hr_dm.shape[0])[1])
        
    hr_ims = np.array(hr_ims)[0]
    gen_ims = np.array(gen_ims)[0]
    hr_ps = hr_ps/xhr.shape[0]
    lr_ps = lr_ps/xlr.shape[0]
    gen_ps = gen_ps/ygen.shape[0]
        
    
    fig, ax = plt.subplots(2,3,figsize=(11,11),frameon=False)
    
    vmin = 1e-2
    vmax = np.max(images[0,0])
    for j in (0,1,2):               
        ax[0,j].imshow(images[0,j], norm=LogNorm(vmin=vmin, vmax=vmax))
        ax[0,j].set_xticks([])  # remove xticks
        ax[0,j].set_yticks([])  # remove yticks
        ax[0,j].axis('off')
            
    ax[1,0].hist([hr_ims,gen_ims], bins=nbins,histtype='step',label=['HR','GEN'],color=['blue','red'])
    ax[1,0].legend()
    ax[1,1].imshow(images[1,0], norm=LogNorm(vmin=np.max(images[1,0])/500, vmax=np.max(images[1,0])))
    ax[1,1].set_xticks([])  # remove xticks
    ax[1,1].set_yticks([])  # remove yticks
    ax[1,1].axis('off')
    if loglogscale:
        ax[1,2].loglog(t,hr_ps,color='blue',label='HR')
        ax[1,2].loglog(t,lr_ps,color='green',label='LR')
        ax[1,2].loglog(t,gen_ps,color='red',label='GEN')
        ax[1,2].set_xlabel('k')
        ax[1,2].set_ylabel('P(k)')
        ax[1,2].legend()
        ax[1,2].set_ylim(np.min(hr_ps)/1.2, np.max(hr_ps)*1.2)
    else:
        ax[1,2].plot(t,hr_ps,color='blue',label='HR')
        ax[1,2].plot(t,gen_ps,color='red',label='GEN')
        ax[1,2].plot(t,lr_ps,color='green',label='LR')
        ax[1,2].set_xlabel('k')
        ax[1,2].set_ylabel('P(k)')
        ax[1,2].legend()
    fig.subplots_adjust(bottom=0.05, top=1, left=0.05, right=0.95, wspace=0.15, hspace=0.05)
    
    if savename is not None:
        fig.savefig(savename, bbox_inches="tight", dpi=dpi)
    if close:
        plt.close()

def plotting_func_bio(xlr, xhr, ygen, hypar=None, **kwargs):
     # do some matplotlib plot, e.g.
    fig, ax = plt.subplots(3,5, figsize=(20,12))
    true = xhr.numpy()
    fake = ygen
    for i in range(0,3):
        ax1 = ax[i,0].imshow(xlr[i,...,0], vmin=0, vmax=1)  # low resolution
        plt.colorbar(ax1, ax=ax[i,0])
        ax2 = ax[i,1].imshow(xhr[i,...,0], vmin=0, vmax=1)  # true high resolution
        plt.colorbar(ax2, ax=ax[i,1])
        ax3 = ax[i,2].imshow(ygen[i,...,0], vmin=0, vmax=1)  # generated
        plt.colorbar(ax3, ax=ax[i,2])
        mse = np.mean((xhr[i]-ygen[i])**2)
        l1 = np.mean(np.abs(xhr[i]-ygen[i]))
        ax[i, 2].set_title(f"MSE: {mse:.3f}\n L1: {l1:.3f}")
        

        ssim, ssim_img = structural_similarity(true[i,...,0], fake[i,...,0], data_range=1.0, full=True)

        ax4 = ax[i,3].imshow(ssim_img)
        plt.colorbar(ax4, ax=ax[i,3])
        ax[i,3].set_title(f"SSIM: {ssim:.2f}")

        MSE_map = np.log(np.square(true[i,...,0] - fake[i,...,0]))

        ax5 = ax[i,4].imshow(MSE_map)
        plt.colorbar(ax5, ax=ax[i,4])

    
def power_spectrum_2d(im,L=205/512*64,n=60,m=1000):
    '''
    Parameters:
    -im : 2D image of which we'll take the power spectrum
    -L : width, in Mpc/h, of the input image. (Depends on patchsize and subsamp!!!)
    -n : into how many bins we discretize the k values (i.e. see how 'step' is defined below)
    -m : how many points to interpolate the function on
    
    Returns:
    -x : the x-axis for the k values, with m spacings
    -interp : the interpolated value of P(k) evaluated at x
    
    '''
    im=im/np.mean(im)
    im_f = np.fft.fftn(im)       
    k_grid = vis.np_get_kmesh(im.shape,L,real=False)
    kmin = np.float32(2*np.pi/L)
    kmax = np.sqrt(2)*kmin*np.ceil((im.shape[0]-1)/2)
    step = (kmax-kmin)/n
    
    numerators = np.zeros(n)
    denominators = np.zeros(n)
    
    for i in range(0,im.shape[0]):
        for j in range(0,im.shape[1]):
            if i==j==0:
                continue
            else:
                p = int((np.linalg.norm(k_grid[i,j])-kmin) // step)
                if p==n:
                    p = n-1
                numerators[p] += im_f[i,j]*np.conjugate(im_f[i,j])
                denominators[p] += 1
    
    x_axis = np.linspace(kmin+step/2,kmax-step/2,n)
    
    while 0 in denominators:
        den=list(denominators)
        num=list(numerators)
        x_ax=list(x_axis)
        k=den.index(0)
        del den[k]
        del num[k]
        del x_ax[k]
        denominators=np.array(den)
        numerators=np.array(num)
        x_axis=np.array(x_ax)
        
    ps = numerators/denominators
    x = np.linspace(kmin+step/2,kmax-step/2,m)
    interp = scp.interpolate.interp1d(x_axis,ps,kind="cubic")(x)

    return x, interp

def plot_samples(generator, xhr, hypar=None, plotting_func=plotting_func_lag2d, savename=None, dpi=None, close=True, return_for_tf=False, noise=True, lr_shownoise=False, noisescale=1., **kwargs):
    
    #assert hypar is not None, "Think about handling this later"
    
    xlr = sg.hr_image_to_lr(xhr, avg=generator.avg, lr_dm_only=generator.lr_dm_only)
    
    
    ygen, z = sg.singan_generate_samples(generator, xlr, noise=noise, return_noisy=True, noisescale=noisescale)
    
    if lr_shownoise:
        #assert generator.addnoise > 0.
        if generator.takelog:
            xlr *= tf.exp(z *  generator.addnoise)
        else:
            xlr += z * generator.addnoise
        
    
    plotting_func(xlr, xhr, ygen, hypar=hypar, **kwargs)
        
    if savename is not None:
        plt.savefig(savename, bbox_inches="tight", dpi=dpi)
    if close:
        plt.close()
        
    if return_for_tf:
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        image = tf.image.decode_png(buf.getvalue(), channels=4)
        image = tf.expand_dims(image, 0)
        
        return image
    

def single_scale_training(hypar, num_iter_gen=2000, checkpoint_frequency=1000, basedir=".", dirlabel="",
                         define_data_sets=None, plotting_func=None, databasedir="../../data", clone=None, cpname=None,  verbose=1, callbacks=[], addlogger_callback=True,
                         generator_fun=sg.define_singan_generator, critic_fun=sg.define_singan_critic, **kwargs):
    if define_data_sets is None:
        define_data_sets = dt.define_datasets
        
    tf.keras.backend.clear_session()
                
    # define models and datasets, restore previous checkpoints

    name = hypar.to_name()
    name = hypar["tag"]
          
    wasserstein, gan, generator, critic, mycp = define_checkpointed_models(hypar, load=True, dirlabel=dirlabel, basedir=basedir, clone=clone, cpname=cpname, verbose=verbose, generator_fun=generator_fun, critic_fun=critic_fun)
    
    if hypar["fakemodel"] != "None":
        wasserstein, gan, generator, critic = None, None, None, None
        startiter = 0
    else:
        startiter = gan.iteration.numpy()+1
    
    np.random.seed(35)
    tf.random.set_seed(42)
    data_gen, data_crit, data_val = define_data_sets(hypar, basedir=databasedir)
    
    val_batch = next(iter(data_val)) #xlr, xlrcent, xhr_nocent, xhr_cent
    
    if plotting_func is not None:
        # In this case add our special plotting callback:
        mycb = cb.PlotOnValidationCallback(plotting_func=plotting_func, hypar=hypar, 
                                           val_batch=val_batch)
        callbacks = callbacks + [mycb]
        
    if addlogger_callback:
        callbacks = callbacks + [cb.MyMetricsLoggerCallback()]

    callbacks.append(cb.QualityCheckPlotCallback(val_batch, step=num_iter_gen))
    it_crit = iter(data_crit)
    it_gen = iter(data_gen)
    
    t0 = time.time()
    
    # when switching to tf 2.4 we can replace this to use tf.keras.callbacks.CallbackList
    callback_list = cb.OurCallbackList(callbacks=callbacks, model=generator)
  
    callback_list.on_train_begin({})
    epoch = 0
    callback_list.on_epoch_begin({})
    
    writer = tf.summary.create_file_writer("%s/logs%s/%s" % (basedir, dirlabel, name))
    with writer.as_default():
        hp.hparams(hypar, trial_id=name)
        for iter_gen in range(startiter, num_iter_gen):
            callback_list.on_train_batch_begin(iter_gen, {})
            
            if iter_gen >= hypar["lrdecrease"]:
                tf.keras.backend.set_value(gan.optimizer.learning_rate, hypar["learningrate"]*0.1)
                tf.keras.backend.set_value(wasserstein.optimizer.learning_rate, hypar["learningrate"]*0.1)

            gan.iteration.assign(iter_gen)

            for i in range(0,hypar["critic_iter"]):
                loss_cr = sg.singan_train_step_critic(generator, wasserstein, next(it_crit))
            loss_gen = sg.singan_train_step_generator(gan, next(it_gen))

            logs = {**dict(zip(wasserstein.loss_names, loss_cr)), 
                    **dict(zip(gan.loss_names, loss_gen))}
                    
            callback_list.on_train_batch_end(iter_gen, logs)

            if (iter_gen % checkpoint_frequency == 0) | (iter_gen == num_iter_gen-1) | (iter_gen == 1):

                print("checkpoints step ", iter_gen)
                t2 = time.time()

                if (iter_gen > 1) & (hypar["fakemodel"] == "None"):
                    ## checkpoint
                    mycp.save(file_prefix= "%s/checkpoints%s/%s/cp" % (basedir, dirlabel, hypar["tag"]))
                    
                callback_list.on_test_begin({})

                ## validation
                callback_list.on_test_batch_begin(iter_gen, {})

                metrics = sg.singan_evaluate_all_metrics(generator, wasserstein, data_val, noise=False, seed=42)
                
                logs = {}
                for key in metrics:
                    logs["test_" + key] = np.mean(metrics[key])

                t3 = time.time()
                callback_list.on_test_batch_end(iter_gen, logs)  # Should have the aggregated test batch results

                print("Metric evaluation took %.1f seconds" % (t3-t2))

                t0 += t3-t2

                writer.flush()

                callback_list.on_test_end(logs)  # Should have the last on_train_batch_end result
                
                callback_list.on_epoch_end(epoch, {})
                epoch += 1
                callback_list.on_epoch_begin(epoch, {})
                
        callback_list.on_train_end({})
                
                
def prediction_multiscale(msgen, xin, scales=(32,16,8,4,2,1), noisescale=1., return_lr_too=True, hrinjection=False):
    if isinstance(xin, dict):
        return prediction_multiscale_multiinjection(msgen, xin, scales=scales, noisescale=noisescale, return_lr_too=return_lr_too, hrinjection=hrinjection)
    else:
        return prediction_multiscale_normal(msgen, xin, scales=scales, noisescale=noisescale, return_lr_too=return_lr_too, hrinjection=hrinjection)
                
def prediction_multiscale_normal(msgen, xin, scales=(32,16,8,4,2,1), noisescale=1., return_lr_too=True, hrinjection=False):
    xlr = xin
    
    xlrup = {}
    xpred = {}
    
    xpred[scales[0]] = xin

    for i,subsamp in enumerate(scales[1:]):
        if hrinjection & (i==0):
            xlrup[subsamp] = xin
        else:
            xlrup[subsamp] = ut.upscale_fields(xpred[subsamp*2], 2)
        xpred[subsamp] = sg.singan_generate_samples(msgen[subsamp], xlrup[subsamp], noise=True, noisescale=noisescale)
        
    if return_lr_too:
        return xlrup, xpred
    else:
        return xpred
                
def prediction_multiscale_multiinjection(msgen, xin, scales=(32,16,8,4,2,1), noisescale=1., return_lr_too=True, hrinjection=False):
    """The input xin is assumed to have this kind of pattern:
    dict: scale => chan, xin
    e.g.
    { 
      8: (0, 1), ndarray(32,32,2)
      4: (1,), ndarray(64,64,1)
      2: (1,), ndarray(128,128,1)
      1: (1,), ndarray(256,256,1)
    }
    The indicated channels will be overwritten by the injected information, the other ones
    will use the upscaled next lower resolution
    
    probably you wanna use assemble_multiinjection_input(...)!
    """
    
    xlr = xin
    
    xlrup = {}
    xpred = {}
    
    def combine_injection(inp, x=None):
        chan, xinj = inp
        assert len(chan) == xinj.shape[-1], "%s %s" % (str(xinj.shape), str(chan))
        if x is None:
            return xinj
        
        assert chan is not None
        
        assert len(chan) <= x.shape[-1]
        
        xnew = np.copy(x)
        
        j = 0
        for c in chan:
            xnew[...,c] = xinj[...,j]
            j += 1
        
        return xnew
    
    if not hrinjection:
        xpred[scales[0]] = combine_injection(xin[scales[0]])
    else:
        xpred[scales[0]] = None

    for i,subsamp in enumerate(scales[1:]):
        if subsamp in xin.keys():
            if hrinjection:
                xup = ut.upscale_fields(xpred[subsamp*2], 2)
                xlrup[subsamp] = combine_injection(xin[subsamp], xup)
            else:
                xlr = combine_injection(xin[subsamp*2], xpred[subsamp*2])
                xlrup[subsamp] = ut.upscale_fields(xlr, 2)
        else:
            xlrup[subsamp] = ut.upscale_fields(xpred[subsamp*2], 2)
        
        xpred[subsamp] = sg.singan_generate_samples(msgen[subsamp], xlrup[subsamp], noise=True, noisescale=noisescale)
        
    if return_lr_too:
        return xlrup, xpred
    else:
        return xpred


def make_generator_movie(generator, outdir, val_batch, hypar=None, subsamp=1, numsamp=10, seed=None, dpi = 120, noisescale=1., num_part_resamp=0, scales=None, plotting_func=plotting_func_lag2d, plot_individually=False,**kwargs):
    tf.random.set_seed(seed)
    
    os.makedirs(outdir, exist_ok=True)
    
    
    xtrue = dt.downscale_image(val_batch, subsamp, random=False, avg=generator.avg)
    
    if plot_individually: # This option is outdated and should be cleaned up
        xlr = sg.hr_image_to_lr(xtrue, avg=generator.avg, lr_dm_only=generator.lr_dm_only)
        xpred_nonoise = sg.singan_generate_samples(generator, xlr, noise=0.)
        
        for mode, label in (("lr", "_lr"), ("hr", "true")): # , ("gen", "gen_nonoise")
            plotting_func(xlr, xtrue, xpred_nonoise, hypar=hypar, mode=mode, savename="%s/image_%s.png" % (outdir, label), close=True, dpi=dpi, **kwargs)
        
        for i in range(0,numsamp):
            xpred, z = sg.singan_generate_samples(generator, xlr, noise=True, noisescale=noisescale, return_noisy=True)
            
            if generator.addnoise > 0.: # In this  case plot lr with added noise
                xlrnoise = xlr + generator.addnoise*z
                #plotting_func(xlrnoise, xtrue, xpred, hypar=hypar, mode="lr", savename="%s/image_lrnoise_%03d.png" % (outdir,i), close=True, dpi=dpi, **kwargs)
            else: # in this case plot the noise map
                xlrnoise = z
                #plotting_func(xlrnoise, xtrue, xpred, hypar=hypar, mode="lrnoise", savename="%s/image_noise_%03d.png" % (outdir,i), close=True, dpi=dpi, **kwargs)
                
            plotting_func(xlr, xtrue, xpred, hypar=hypar, mode="gen", savename="%s/image_gen_%03d.png" % (outdir,i), close=True, dpi=dpi, **kwargs)
    else:
        plot_samples(generator, xtrue, plotting_func=plotting_func, hypar=hypar, noise=False, savename="%s/image_nonoise.png" % outdir, close=True, dpi=dpi, nsamp=num_part_resamp, **kwargs)
        
        for i in range(0,numsamp):
            plot_samples(generator, xtrue, plotting_func=plotting_func, hypar=hypar, noise=True, savename="%s/image_gen_%03d.png" % (outdir,i), close=True, dpi=dpi, nsamp=num_part_resamp, lr_shownoise=True, **kwargs)

def assemble_multiinjection_input(msgen, xhr, scales):
    inputdict = {}
    for scale in scales:
        if scale == scales[0]:
            chan = (0,1)
            if scale in msgen:
                avg = msgen[scale].avg
            else:
                assert scale//2 in msgen, "Didn't think this case through (also shouldn't happen I think)"
                avg = msgen[scale//2].avg
            xin = dt.downscale_image(xhr, scale, avg=avg)
        else:
            chan = msgen[scale].outputmask
            xin = dt.downscale_image(xhr, scale, avg=msgen[scale].avg)[...,chan]
        inputdict[scale] = chan, xin
    return inputdict
            
def make_multiscale_movie(msgen, outdir, xhr=None, xin=None, scales=(32,16,8,4,2,1), hypars=None, plotting_func=plotting_func_lag2d, numsamp=10, seed=None, dpi = 120, noisescale=1., num_part_resamp=0, prefix="",  hrinjection=False, plot_individually=False, **kwargs):
    tf.random.set_seed(seed)
    
    for subsamp in scales[1:]:
        os.makedirs("%s/%smulti_scale_%02d_inj%02d" % (outdir, prefix, subsamp, scales[0]), exist_ok=True)
        

    if xin is None:
        using_output_masks = False
        for key in msgen:
            if msgen[key].outputmask is not None:
                if len(msgen[key].outputmask) > 0:
                    using_output_masks = True
        
        if not using_output_masks:
            assert xhr is not None
            if hrinjection:
                xin = dt.downscale_image(xhr, scales[1], random=False, avg=msgen[scales[1]].avg)
            else:
                xin = dt.downscale_image(xhr, scales[0], random=False, avg=msgen[scales[1]].avg)
            xinlr = xin
        else:
            if hrinjection:
                xin = assemble_multiinjection_input(msgen, xhr, scales[1:])
                xinlr = xin[scales[1]][1]
            else:
                xin = assemble_multiinjection_input(msgen, xhr, scales[0:])
                xinlr = xin[scales[0]][1]
    
    
    for i in range(0,numsamp):
        xlrup, xpred = prediction_multiscale(msgen, xin, noisescale=noisescale, scales=scales, hrinjection=hrinjection)
        for subsamp in scales[1:]:
            if xhr is not None:
                xhrscale = dt.downscale_image(xhr, subsamp, random=False, avg=msgen[subsamp].avg)
            else:
                xhrscale = None

            if hypars is not None:
                hypar = hypars[subsamp]
            else:
                hypar = None
                
            savename = "%s/%smulti_scale_%02d_inj%02d/image_gen_%03d.png" % (outdir,prefix,subsamp,scales[0],i)
            plotting_func(xinlr, xhrscale, xpred[subsamp], hypar=hypar, dpi=dpi, savename=savename, close=True, **kwargs)
            
            if plot_individually & (i==0): # Plot the true hr and lr once
                savename = "%s/%smulti_scale_%02d_inj%02d/image__lr_%03d.png" % (outdir,prefix,subsamp,scales[0],i)
                plotting_func(xinlr, xhrscale, xpred[subsamp], hypar=hypar, dpi=dpi, savename=savename, close=True, mode="lr", **kwargs)
                
                savename = "%s/%smulti_scale_%02d_inj%02d/image_true_%03d.png" % (outdir,prefix,subsamp,scales[0],i)
                plotting_func(xinlr, xhrscale, xpred[subsamp], hypar=hypar, dpi=dpi, savename=savename, close=True, mode="hr", **kwargs)

            
def get_latest_checkpoint_filename(hypar, dirlabel=""):
    name = hypar.to_name()
    name = hypar["tag"]
    
    checkpointdir = "checkpoints%s/%s" % (dirlabel, name)

    latest_checkpoint = tf.train.latest_checkpoint(checkpointdir)
    
    return latest_checkpoint
            
class MultiscaleModel():
    def __init__(self, hypars, dirlabel="_multiscale", label="multiscale", basedir=".", databasedir="../../data", define_data_sets=None):
        self.hypars = hypars
        self.scales = np.sort(list(hypars.keys()))[::-1]  # have to be sorted descending
        self.label = label
        
        self.dirlabel = dirlabel
        
        self.generators = None
        
        self.basedir = basedir
        self.databasedir = databasedir
        self.define_data_sets = define_data_sets
        
    def do_training(self, num_iter_gen, checkpoint_frequency=1000, plotting_func=plotting_func_lag2d, clones=None, scales=None, cpname=None, verbose=1, callbacks=[], **kwargs):
        if scales is None:
            scales = self.scales
        for scale in self.scales:
            if clones is not None:
                clone = clones[scale]
            else:
                clone = None
                
                
            single_scale_training(self.hypars[scale], checkpoint_frequency=checkpoint_frequency, num_iter_gen=num_iter_gen, basedir=self.basedir,
                                  dirlabel=self.dirlabel, define_data_sets=self.define_data_sets, plotting_func=plotting_func, databasedir=self.databasedir,
                                  clone=clone, cpname=cpname, verbose=verbose, callbacks=callbacks, **kwargs)
            
    def load_all_generators(self, scales=None, cpname=None, verbose=1): #won't need right now
        
        if scales is None:
            scales = self.scales
        
        msgen = {}
        for scale in scales:
            wasserstein, gan, generator, critic, mycp = define_checkpointed_models(self.hypars[scale], load=True, dirlabel=self.dirlabel, for_training=False, basedir=self.basedir, cpname=cpname, verbose=verbose) 
            #assert gan.iteration.numpy() > 0 !!comment this back in later!!
            msgen[scale] = generator
            
        self.generators = msgen
        return msgen
    
    def prediction_multiscale(self, xin, scales=None, noisescale=1., return_lr_too=False, hrinjection=False, cpname=None):
        if (self.generators is None) | (cpname is not None):
            self.load_all_generators(scales=scales, cpname=cpname)
            
        if scales is None:
            scales = self.scales
        
        return prediction_multiscale(self.generators, xin, scales=scales, noisescale=noisescale, return_lr_too=return_lr_too, hrinjection=hrinjection)
    
    def get_checkpoint_list(self, scales=None, print_for_git=False):
        if scales is None:
            scales = self.scales
            
        cpfiles = {}
        for scale in scales:
            cpfiles[scale] = get_latest_checkpoint_filename(self.hypars[scale], dirlabel=self.dirlabel)
            
        if print_for_git:
            for scale in scales:
                print("git add %s/checkpoint" % os.path.dirname(cpfiles[scale]))
                print("git add %s*" % cpfiles[scale])
            
        return cpfiles
        
        
    
    def make_movies(self, xhr, xin=None, moviesdir=None, label=None, plotting_func=plotting_func_lag2d, scales=None, numsamp=10, noisescale=1., dpi=120, mode="both", injection_scales=None, plot_individually=False, hrinjection=False, cpname=None, framerate=1, **kwargs):
        
        if moviesdir is None:
            moviesdir = "%s/movies"  % self.basedir
        
        if scales is None:
            scales = self.scales
        if label is None:
            label = self.label
            
        if (self.generators is None) | (cpname is not None):
            self.load_all_generators(scales=scales, cpname=cpname)
            
        refhp = self.hypars[scales[0]]
        
        def make_ffmpegstr(scales, mscale_inj=None):
            mystr = "\nfor i in "
            for i in scales:
                mystr += "%02d " % i
            mystr +="\ndo\n"
            if mscale_inj is None:
                #image_gen_%03d.png
                mystr += "ffmpeg -y -framerate %g -i \"single_scale_${i}/%%*.png\"  %s_singlescale_${i}.gif" % (framerate, label)
            else:
                #image%%03d.png
                mystr += "ffmpeg -y -framerate %g -i \"multi_scale_${i}_inj%02d/%%*.png\"  %s_multiscale_${i}_inj%02d.gif" % (framerate, mscale_inj, label, mscale_inj)
            return mystr + "\ndone\n"
        
        ffmpegstr = ""
        
        if (mode == "multi") | (mode == "both"):
            multiscales = np.array((scales[0]*2,) + tuple(scales))
            if injection_scales is None:
                injection_scales = (multiscales[0],)
            for inscale in injection_scales:
                ffmpegstr += make_ffmpegstr(multiscales[multiscales < inscale], mscale_inj=inscale)
                
                make_multiscale_movie(self.generators, "%s/%s"  % (moviesdir, label), xhr=xhr, xin=xin, hypars=self.hypars, plotting_func=plotting_func,
                                      scales=multiscales[multiscales <= inscale], numsamp=numsamp, dpi = dpi, noisescale=noisescale,
                                      hrinjection=hrinjection, plot_individually=plot_individually, **kwargs)
                
                
                
                
        if (mode == "single") | (mode == "both"):
            for scale in scales:
                outdir = "%s/%s/single_scale_%02d" % (moviesdir, label, scale)
                make_generator_movie(self.generators[scale], outdir, xhr, hypar=self.hypars[scale], subsamp=scale, numsamp=numsamp, plotting_func=plotting_func, plot_individually=plot_individually, 
                                     seed=None, dpi = dpi, noisescale=noisescale, **kwargs)
            ffmpegstr += make_ffmpegstr(scales)

                
        with open("%s/%s/makeallmov.sh" % (moviesdir, label), "w") as f:
            f.write(ffmpegstr)
        import os
        os.chmod("%s/%s/makeallmov.sh" % (moviesdir, label), 0o777)
