from datasets_bio import *
import matplotlib.pyplot as plt

gen, val = bio_define_datasets("data/EM/train", 4, 4)

img = gen.as_numpy_iterator().next()
print(img.shape)

plt.figure(figsize=(10,10))
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.imshow(img[i], vmin=0, vmax=1, cmap='gray')
# show the figure
plt.show()