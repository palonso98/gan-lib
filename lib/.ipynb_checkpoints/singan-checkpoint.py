import tensorflow as tf
import tensorflow.keras.layers as layers
import numpy as np
from tensorflow.python.framework.ops import Tensor
from SpectralNormalization import SpectralNormalization 
from skimage.transform import downscale_local_mean
from skimage.metrics import structural_similarity, peak_signal_noise_ratio
import datasets_bio as dt

def conv_block(inputs, nchan, ndim=2, strides=1, upconv=False, alpha=0.2, activation="none",spectral_norm=True, batch_norm=False):
    x = inputs
    
    if ndim == 1:
        ConvLayer = layers.Conv1D
        ConvTransposeLayer = layers.Conv1DTranspose
    if ndim == 2:
        ConvLayer = layers.Conv2D
        ConvTransposeLayer = layers.Conv2DTranspose
    elif ndim == 3:
        ConvLayer = layers.Conv3D
        ConvTransposeLayer = layers.Conv3DTranspose
    else:
        raise ValueError("ndim = %d, Are you sure? I am not ready for GR yet nor point like spaces" % ndim) 
        
    filtersize = (3,) * ndim

    if upconv:
        if spectral_norm:
            x = SpectralNormalization(ConvTransposeLayer(nchan, filtersize, strides=strides, padding="same"))(x)
        else:
            x = ConvTransposeLayer(nchan, filtersize, strides=strides, padding="same")(x)
    else:
        if spectral_norm:
            x = SpectralNormalization(ConvLayer(nchan, filtersize, strides=strides, padding="same"))(x)
        else:
            x = ConvLayer(nchan, filtersize, strides=strides, padding="same")(x)
            
    if batch_norm == True:
        x =  layers.BatchNormalization(axis=-1)(x)
    elif batch_norm == "layer":
        x =  layers.LayerNormalization(axis=-1)(x)


    if activation is not None:
        if activation == "leaky_relu":
            x = layers.LeakyReLU(alpha=alpha)(x)
        else:
            x = layers.Activation(activation)(x)

    return x

def mask_channels(x, inputmask=None):
        
    if inputmask is not None:
        if len(inputmask) == 0:
            return x
        
        mychan = []
        for i in range(x.shape[-1]):
            if not i in inputmask:
                mychan.append(x[...,i:i+1])
        if len(mychan) > 1:
            return layers.Concatenate(axis=-1)(mychan)
        else:
            return mychan[0]
    else:
        return x

def define_singan_generator_residual(imext=None, name="generator", ndim=2, nchan=2, d=32, spectral_norm=False, addnoise=0., nconvlayers=5, batch_norm=False, takelog=False, outputmask=[], inputmask=[], activation="tanh", addlr="auto", avg=False, lr_dm_only="auto", sigma=None):
    # xin: the low resolution image scaled up
    # zin: noise of the same shape like the input image
    
    xin = tf.keras.layers.Input(shape=(imext,)*ndim + (nchan,))
    zin = tf.keras.layers.Input(shape=(imext,)*ndim + (nchan,))

    inputs = [xin, zin]
    
    if takelog:
        xin = tf.math.log(xin)
    
    if addnoise > 0.:
        x = xin + zin * addnoise
    else:
        x = layers.Concatenate(axis=-1)([xin, zin])
        
    if len(inputmask) > 0:
        x = mask_channels(x, inputmask)
    else:
        inputmask=[]
        
    #print("Mask: ", inputmask, " # Channels: %d" % x.shape[-1])

    def res_block(inputs, nchan):
        x = layers.Conv2D(nchan, kernel_size=(3,3), strides=(1,1), padding='same', activation='relu')(inputs)
        x = layers.Conv2D(nchan, kernel_size=(3,3), strides=(1,1), padding='same', activation=None)(x)
        x = layers.Add()([x, inputs])
        return x

    def my_network(x, nchanout=nchan):
        c1 = layers.Conv2D(d, kernel_size=(7,7), strides=(1,1), padding='same', activation=None)(x)

        x = res_block(c1, d)
        for i in range(0, nconvlayers-1):
            x = res_block(x, d)

        c2 = layers.Conv2D(d, kernel_size=(3,3), strides=(1,1), padding='same', activation=None)(x)

        x = layers.Add()([c1, c2]) # critic?

        x = layers.Conv2D(d, kernel_size=(5,5), strides=(1,1), padding='same', activation='relu')(x)

        x = layers.Conv2D(nchanout, kernel_size=(1,1), strides=(1,1), activation=activation)(x)

        return x
    
    if (len(outputmask) == 0) & (len(inputmask) == 0):
        x = my_network(x, nchanout=nchan)
        if (not addlr) | (addlr == "False"):
            pass
        else:
            x = xin + x
    else:
        x = my_network(x, nchanout=nchan-len(outputmask))
        myx = []
        j = 0
        for i in range(0, nchan):
            if i in outputmask:
                #assert addlr == "auto", "Not thought about this combination yet"
                myx.append(xin[...,i:i+1])
            elif i in inputmask:
                #assert addlr == "auto", "Not thought about this combination yet"
                myx.append(x[...,j:j+1])
                j += 1
            else:
                if (not addlr) | (addlr == "False"):
                    myx.append(x[...,j:j+1])
                else:
                    myx.append(xin[...,i:i+1] + x[...,j:j+1])
                j += 1
        x = tf.keras.layers.concatenate(myx, axis=-1)
    
    if takelog:
        x = tf.exp(x)
    
    model = tf.keras.Model(inputs=inputs, outputs=x, name=name)
    
    model.ndim = ndim
    model.takelog = takelog
    model.addnoise = addnoise
    model.inputmask = inputmask
    model.outputmask = outputmask
    model.sigma = sigma
    
    # This paramter says whether this generator expects the input images to be downscaled
    # by subsampling or by averaging:
    model.avg = avg 
    
    if lr_dm_only == "auto":
        if 1 in model.outputmask:
            model.lr_dm_only = True
        else:
            model.lr_dm_only = False
    else:
        model.lr_dm_only = lr_dm_only
    
    return model

def define_singan_generator(imext=None, name="generator", ndim=2, nchan=2, d=32, spectral_norm=False, addnoise=0., nconvlayers=5, batch_norm=False, takelog=False, outputmask=[], inputmask=[], activation="tanh", addlr="auto", avg=False, lr_dm_only="auto", sigma=None):
    # xin: the low resolution image scaled up
    # zin: noise of the same shape like the input image
    
    xin = tf.keras.layers.Input(shape=(imext,)*ndim + (nchan,))
    zin = tf.keras.layers.Input(shape=(imext,)*ndim + (nchan,))

    inputs = [xin, zin]
    
    if takelog:
        xin = tf.math.log(xin)
    
    if addnoise > 0.:
        x = xin + zin * addnoise
    else:
        x = layers.Concatenate(axis=-1)([xin, zin])
        
    if len(inputmask) > 0:
        x = mask_channels(x, inputmask)
    else:
        inputmask=[]
        
    #print("Mask: ", inputmask, " # Channels: %d" % x.shape[-1])
        
    def my_network(x, nchanout=nchan):
        for i in range(0, nconvlayers-1):
            x = conv_block(x, d, ndim=ndim, strides=1, spectral_norm=spectral_norm, activation="leaky_relu", batch_norm=batch_norm)

        x = conv_block(x, nchanout, ndim=ndim, strides=1, spectral_norm=spectral_norm, activation=activation, batch_norm=False)

        return x
    
    if (len(outputmask) == 0) & (len(inputmask) == 0):
        x = my_network(x, nchanout=nchan)
        if (not addlr) | (addlr == "False"):
            pass
        else:
            x = xin + x
    else:
        x = my_network(x, nchanout=nchan-len(outputmask))
        myx = []
        j = 0
        for i in range(0, nchan):
            if i in outputmask:
                #assert addlr == "auto", "Not thought about this combination yet"
                myx.append(xin[...,i:i+1])
            elif i in inputmask:
                #assert addlr == "auto", "Not thought about this combination yet"
                myx.append(x[...,j:j+1])
                j += 1
            else:
                if (not addlr) | (addlr == "False"):
                    myx.append(x[...,j:j+1])
                else:
                    myx.append(xin[...,i:i+1] + x[...,j:j+1])
                j += 1
        x = tf.keras.layers.concatenate(myx, axis=-1)
    
    if takelog:
        x = tf.exp(x)
    
    model = tf.keras.Model(inputs=inputs, outputs=x, name=name)
    
    model.ndim = ndim
    model.takelog = takelog
    model.addnoise = addnoise
    model.inputmask = inputmask
    model.outputmask = outputmask
    model.sigma = sigma
    
    # This paramter says whether this generator expects the input images to be downscaled
    # by subsampling or by averaging:
    model.avg = avg 
    
    if lr_dm_only == "auto":
        if 1 in model.outputmask:
            model.lr_dm_only = True
        else:
            model.lr_dm_only = False
    else:
        model.lr_dm_only = lr_dm_only
    
    return model

class SinGanGeneratorNoiseGradientModel(tf.keras.models.Model):
    """This thing takes a generator and makes him output the gradient with
    respect to the input noise maps additionally"""
    def __init__(self, generator):
        super(SinGanGradientModel, self).__init__()
        self.generator = generator

    @tf.function
    def call(self, input_data,  training=None):
        xin, zin = input_data
        
        y0 = self.generator([xin, zin*0.])
        y = self.generator([xin, zin])
        
        #std = mean((ys-y0)**2) - noiselevel**2
        
        #mean((ys-y0)**2) - noiselevel**2
        
        #ystd = tf.reduce_mean(y**2)
        
        #var(f) ~ mean((df**2/dn0)) * std(n0)
        
        # var(f) ~ mean((df/dn0)**2) * std(n0)**2
        # minimize(var(f) - noiselevel**2)
        
        # yvector
        # ylength = sqrt(sum(y**2))
        
        tf.gradients(np.mean(ystd**2), zin)

        # wasserstein metric
        Dtrue = self.critic(ytrue_in, training=training)
        Dfake = self.critic(yfake_in, training=training)
        
        Dprod = layers.ReLU()(Dtrue * Dfake)
        
        ws_metric = Dtrue - Dfake
        
        # gradient penalty
        # create a random image somewhere between true and fake ones to test the gradients
        # at random locations
        alpha = tf.random.uniform(shape=(1,), minval=0, maxval=1.)
        
        yalpha = alpha * ytrue + (alpha-1.) * yfake
        
        if self.input_lr_too:
            yalpha_in = yalpha, ylr
        else:
            yalpha_in = yalpha
        
        Dalpha = self.critic(yalpha_in, training=training)
        grad = tf.gradients(Dalpha, yalpha)
        
        # Note that [0] is there because tensorflow reshapes to (1,...) in the beginning for some unknown reason...
        gradabs = tf.sqrt(tf.reduce_mean(tf.square(grad), axis=(-1,-2,-3)), name="myloss")[0]

        return ws_metric, gradabs, Dprod
    
    def get_loss_functions(self, lam=10., stabilization_loss=0.):
        def loss_wsm(y_true, y_pred):
            wsmetric = y_pred
            return  -tf.math.reduce_mean(wsmetric)
        
        def loss_gp(y_true, y_pred):
            gradabs = y_pred
            gradient_panelty = lam*tf.math.reduce_mean(tf.square(gradabs - 1.))

            return gradient_panelty
        
        def loss_stabilization(y_true, y_pred):
            return stabilization_loss*y_pred
        
        return [loss_wsm, loss_gp, loss_stabilization]
    
#separate_channels=False, 
def define_singan_critic(name="critic", ndim=2, nchan=2, d=32, input_lr_too=False, spectral_norm=False, addnoise=0., nconvlayers=5, batch_norm=False, subtractmean=True, imext=None, takelog=False, inputmask=[]):
    ## imext only for debug purposes
    
    shape = (imext,)*ndim + (nchan,)    
    
    # yin: the high resolution true or fake image
    if input_lr_too:
        yhr = tf.keras.layers.Input(shape=shape)
        ylr = tf.keras.layers.Input(shape=shape)
        
        yin = [yhr, ylr]
        
        if takelog:
            yhr,ylr = tf.math.log(yhr), tf.math.log(ylr)

        if subtractmean:
            y0 = layers.GlobalAveragePooling2D()(ylr)[...,tf.newaxis,tf.newaxis,:]
            x =  layers.Concatenate(axis=-1)([yhr-y0, ylr-y0, yhr-ylr])
        else:
            x =  layers.Concatenate(axis=-1)([yhr, ylr, yhr-ylr])
    else:
        yin = tf.keras.layers.Input(shape=shape)
        if takelog:
            y = tf.math.log(yin)
        else:
            y = yin

        if subtractmean:
            if ndim == 1:
                y0 = layers.GlobalAveragePooling1D()(y)[...,tf.newaxis,:]
            elif ndim == 2:
                y0 = layers.GlobalAveragePooling2D()(y)[...,tf.newaxis,tf.newaxis,:]
            elif ndim == 3:
                y0 = layers.GlobalAveragePooling3D()(y)[...,tf.newaxis,tf.newaxis,tf.newaxis,:]
            else:
                raise ValueError("ndim = %d ... are you a Point-Earther?" % ndim);
            x = y - y0
        else:
            x = y
            
    def my_network(x):
        for i in range(0, nconvlayers-1):
            x = conv_block(x, d, ndim=ndim, strides=1, spectral_norm=spectral_norm, activation="leaky_relu", batch_norm=batch_norm)

        x = conv_block(x, 1, ndim=ndim, strides=1, spectral_norm=False, activation=None, batch_norm=False)
        return x
     
    x = mask_channels(x, inputmask)
    
    Di = my_network(x)
    
    model = tf.keras.Model(inputs=yin, outputs=Di, name=name)
    
    model.ndim = ndim
    model.nchan = nchan
    model.input_lr_too = input_lr_too
    model.takelog = takelog
    
    model.inputmask = inputmask
    
    return model

def define_singan_critic_residual(name="critic", ndim=2, nchan=2, d=32, input_lr_too=False, spectral_norm=False, addnoise=0., nconvlayers=5, batch_norm=False, subtractmean=True, imext=None, takelog=False, inputmask=[]):
    ## imext only for debug purposes
    
    shape = (imext,)*ndim + (nchan,)
    
    # yin: the high resolution true or fake image
    if input_lr_too:
        yhr = tf.keras.layers.Input(shape=shape)
        ylr = tf.keras.layers.Input(shape=shape)
        
        yin = [yhr, ylr]
        
        if takelog:
            yhr,ylr = tf.math.log(yhr), tf.math.log(ylr)

        if subtractmean:
            y0 = layers.GlobalAveragePooling2D()(ylr)[...,tf.newaxis,tf.newaxis,:]
            x =  layers.Concatenate(axis=-1)([yhr-y0, ylr-y0, yhr-ylr])
        else:
            x =  layers.Concatenate(axis=-1)([yhr, ylr, yhr-ylr])
    else:
        yin = tf.keras.layers.Input(shape=shape)
        if takelog:
            y = tf.math.log(yin)
        else:
            y = yin

        if subtractmean:
            if ndim == 1:
                y0 = layers.GlobalAveragePooling1D()(y)[...,tf.newaxis,:]
            elif ndim == 2:
                y0 = layers.GlobalAveragePooling2D()(y)[...,tf.newaxis,tf.newaxis,:]
            elif ndim == 3:
                y0 = layers.GlobalAveragePooling3D()(y)[...,tf.newaxis,tf.newaxis,tf.newaxis,:]
            else:
                raise ValueError("ndim = %d ... are you a Point-Earther?" % ndim);
            x = y - y0
        else:
            x = y
            
    def res_block(inputs, nchan):
        x = layers.Conv2D(nchan, kernel_size=(3,3), strides=(1,1), padding='same', activation='relu')(inputs)
        x = layers.Conv2D(nchan, kernel_size=(3,3), strides=(1,1), padding='same', activation=None)(x)
        x = layers.Add()([x, inputs])
        return x

    def my_network(x, nchanout=nchan):
        c1 = layers.Conv2D(d, kernel_size=(7,7), strides=(1,1), padding='same', activation=None)(x)

        x = res_block(c1, d)
        for i in range(0, nconvlayers-1):
            x = res_block(x, d)

        c2 = layers.Conv2D(d, kernel_size=(3,3), strides=(1,1), padding='same', activation='relu')(x)

        x = layers.Conv2D(nchanout, kernel_size=(1,1), strides=(1,1), activation=None)(x)

        return x
     
    x = mask_channels(x, inputmask)
    
    Di = my_network(x)
    
    model = tf.keras.Model(inputs=yin, outputs=Di, name=name)
    
    model.ndim = ndim
    model.nchan = nchan
    model.input_lr_too = input_lr_too
    model.takelog = takelog
    
    model.inputmask = inputmask
    
    return model

class SinGanWassersteinModel(tf.keras.models.Model):
    def __init__(self, critic, takelog_grad=False):
        super(SinGanWassersteinModel, self).__init__()
        self.critic = critic
        self.ndim = critic.ndim
        self.input_lr_too = critic.input_lr_too
        self.takelog = self.critic.takelog
        
        self.loss_names = ["cr_loss", "cr_wasserstein", "cr_gradient", "cr_stabilization"]

    @tf.function
    def call(self, input_data,  training=None):
        if self.input_lr_too:
            ytrue, yfake, ylr = input_data
            ytrue_in = [ytrue, ylr]
            yfake_in = [yfake, ylr]
        else:
            ytrue, yfake = input_data
            ytrue_in, yfake_in = ytrue, yfake
            
        alpha = tf.random.uniform(shape=(1,), minval=0, maxval=1.)
        #print("Alpha", alpha.shape)
        if self.takelog:
            #ytrue_in, yfake_in = tf.math.log(ytrue_in), tf.math.log(yfake_in)
            tf.Assert(tf.greater_equal(tf.reduce_min(ytrue_in), 1e-11), [tf.reduce_min(ytrue_in)])
            tf.Assert(tf.greater_equal(tf.reduce_min(yfake_in),  1e-11), [tf.reduce_min(yfake_in)])
            
            logyalpha = alpha * tf.math.log(ytrue_in) + (1.-alpha) * tf.math.log(yfake_in)
            yalpha = tf.exp(logyalpha)
        else:
            yalpha = alpha * ytrue_in + (1.-alpha) * yfake_in
            
        if self.ndim == 1:
            imageaxes = (-1,)
            imageaxes_grad = (-1,-2)
        elif self.ndim == 2:
            imageaxes = (-1,-2)
            imageaxes_grad = (-1,-2,-3)
        elif self.ndim == 3:
            imageaxes = (-1,-2,-3)
            imageaxes_grad = (-1,-2,-3,-4)

        # wasserstein metric
        # average over all pixel classifications
        Dtrue = tf.reduce_mean(self.critic(ytrue_in, training=training), axis=imageaxes)
        #print("Dtrue", Dtrue.shape)
        Dfake = tf.reduce_mean(self.critic(yfake_in, training=training), axis=imageaxes)
        #print("Dfake", Dfake.shape)
        
        Dprod = layers.ReLU()(Dtrue * Dfake)
        #print("Dprod", Dprod.shape)
        
        ws_metric = Dtrue - Dfake
        #print("ws_metric", ws_metric.shape)
        
        # gradient penalty
        # create a random image somewhere between true and fake ones to test the gradients
        # at random locations
       
        
        if self.input_lr_too:
            yalpha_in = yalpha, ylr
        else:
            yalpha_in = yalpha
        
        # average over all pixel classifications
        #Dalpha = tf.reduce_mean(self.critic(yalpha_in, training=training), axis=imageaxes)
        Dalpha = self.critic(yalpha_in, training=training)
        # average the gradient also over batches
        #print("Dalpha", Dalpha.shape)
        

        if self.takelog:
            grad = tf.gradients(tf.reduce_mean(Dalpha), logyalpha)[0]
        else:
            grad = tf.gradients(Dalpha, yalpha)[0]
            
        if len(self.critic.inputmask) > 0:
            grad = mask_channels(grad, self.critic.inputmask)
        
        #print("grad", grad.shape)
        #print("imageaxis", imageaxes_grad)
        gradabs = tf.sqrt(tf.reduce_sum(tf.square(grad), axis=imageaxes_grad), name="myloss")
        
        #print("grad", grad.shape)
        #grad_view = tf.reshape(grad, [tf.shape(grad)[0], -1])
        #gradabs = tf.norm(grad_view, ord=2, axis=1)
        #print("gradabs", gradabs.shape)

        return ws_metric, gradabs, Dprod
    
    def get_loss_functions(self, lam=10., stabilization_loss=0., gradient=1., gradfactor=1.):
        def loss_wsm(y_true, y_pred):
            wsmetric = y_pred
            return  -tf.math.reduce_mean(wsmetric)
        
        def loss_gp(y_true, y_pred):
            gradabs = y_pred
            gradient_penalty = lam*tf.math.reduce_mean(tf.square(gradabs * gradfactor - gradient)) # TODO: hypar

            return gradient_penalty
        
        def loss_stabilization(y_true, y_pred):
            return stabilization_loss*y_pred
        
        return [loss_wsm, loss_gp, loss_stabilization]
    
def singan_model(generator, critic, imext=None, optimizer="adam", recloss_coeff=0., 
                 recloss_type="l2zeronoise", noise_penalty_stddev=0., noise_penalty_coeff=0.):
    assert critic.trainable == False, "the gan needs to use an untrainable critic"
    
    ndim = critic.ndim
    nchan = critic.nchan
    
    shape = (imext,)*ndim + (nchan,)
    
    z = tf.keras.Input(shape=shape)
    xin = tf.keras.Input(shape=shape)
    
    inputs = [xin, z]
    
    if recloss_coeff > 0.:
        ytrue = tf.keras.Input(shape=shape)
        inputs.append(ytrue)
    
    # connect them
    yfake = generator([xin, z])
    
    if critic.input_lr_too:
        Dfake = critic([yfake, xin])
    else:
        Dfake = critic(yfake)
        
    outputs = [Dfake]
    if noise_penalty_coeff > 0.:
        yzeronoise = generator([xin, z*0.])
        
        if generator.takelog:
            outputs.append(tf.math.log(yfake)-tf.math.log(yzeronoise))
        else:
            outputs.append(yfake-yzeronoise)
    else:
        yzeronoise = None
        
    if recloss_coeff > 0.:
        if "zeronoise" in recloss_type:
            if yzeronoise is None:
                yzeronoise = generator([xin, z*0.])
            yrec = yzeronoise
        else:
            yrec = generator([xin, z])
        
        if generator.takelog:
            dyrec = tf.math.log(yrec)-tf.math.log(ytrue)
        else:
            dyrec = yrec-ytrue
            
        if len(generator.outputmask) > 0:
            dyrec = mask_channels(dyrec, generator.outputmask)
            
        outputs.append(dyrec)
    
    
    num_outputs = len(outputs)
    if len(outputs) == 1:
        outputs = outputs[0]
    
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    
    def wasserstein_loss(y_true, y_pred):
        # need to have y_true as argument, allthough we are not gonna use it
        return -tf.reduce_mean(y_pred) # minimize wasserstein_metric
    if "l2" in recloss_type:
        def reconstruction_loss(y_true, y_pred):
            return tf.reduce_mean(recloss_coeff * y_pred**2) 
    else:
        assert "l1" in recloss_type
        def reconstruction_loss(y_true, y_pred):
            return tf.reduce_mean(recloss_coeff * tf.abs(y_pred) )
        
    def noise_penalty_loss(y_true, y_pred):
        return tf.reduce_mean(noise_penalty_coeff*(y_pred**2 - noise_penalty_stddev**2.)**2)
    
    #"cr_loss", "cr_wasserstein", "cr_gradient", "cr_stabilization"
    losses = [wasserstein_loss]
    loss_names = ["wasserstein"]
    
    if noise_penalty_coeff > 0.:
        #assert not generator.takelog, "Not implemented this feature for takelog=True, yet"
        losses.append(noise_penalty_loss)
        loss_names.append("gan_noise_penalty")
    if recloss_coeff > 0.:
        losses.append(reconstruction_loss)
        loss_names.append("reconstruction")
        
    if len(losses) > 1:
        # prepend the first entry which is always the total loss
        loss_names = ["gan_loss"] + loss_names
     
    # if(len(losses)) == 1
    #losses = losses[0]
    
    model.loss_names = loss_names

    model.compile(loss=losses, optimizer=optimizer)
    
    # reconstruction_loss
    
    #if recloss_coeff > 0.:
    #    model.compile(loss=[wasserstein_loss, reconstruction_loss], optimizer=optimizer)
    #else:
    #    model.compile(loss=wasserstein_loss, optimizer=optimizer)
    
    model.ndim = ndim
    model.iteration = tf.Variable(0, dtype=tf.int64, trainable=False)
    
    model.generator = generator
    model.critic = critic
    
    model.recloss_coeff = recloss_coeff
    model.noise_penalty_stddev = noise_penalty_stddev
    model.noise_penalty_coeff = noise_penalty_coeff
    model.num_outputs = num_outputs    
    
    return model


def hr_image_to_lr(xhr, avg = 0, lr_dm_only=False, ndim=2, sigma=1.5):
    '''
    avg: if true, the hr image is downscaled by a factor of 2 by taking the local average.
    Make sure the shape of the input is the right one
    '''
    xlr = np.copy(xhr)
    
    if avg == 1:
        if(len(xhr.shape) >= 5):
            raise NotImplementedError("I have only implemented this for ndim=2 so far")
        
        for n in range(0,xhr.shape[0]):
            im0 = downscale_local_mean(xhr[n,:,:,0],(2,2))
            im1 = downscale_local_mean(xhr[n,:,:,1],(2,2))
            for i in (0,1):
                for j in (0,1):
                    xlr[n,i::2,j::2,0]=im0
                    if not lr_dm_only:
                        xlr[n,i::2,j::2,1]=im1 
                
    elif avg == 0:
        if ndim==1:
            for i in (0,1):
                xlr[...,i::2,:] = xhr[...,::2,:]
        elif ndim==2:
            for i in (0,1):
                for j in (0,1):
                    xlr[...,i::2,j::2,:] = xhr[...,::2,::2,:]
        elif ndim==3:
            for i in (0,1):
                for j in (0,1):
                    for k in (0,1):
                        xlr[...,i::2,j::2,k::2,:] = xhr[...,::2,::2,::2,:]
        else:
            raise ValueError("ndim = %3 ? This is not a black hole!" % ndim)
    else:
        xlr = dt.hr_to_lr_reduce_size(xhr, sigma=sigma, down_factor=4)
            
    return xlr


def singan_generator_noise(shape, noise=True):
    if noise:
        z = tf.random.normal(shape=shape)
    else:
        z = tf.zeros(shape=shape)
    
    return z

def singan_train_step_generator(gan, databatch):
    my_xhr = databatch
    my_xlr = hr_image_to_lr(my_xhr,avg=gan.generator.avg,lr_dm_only=gan.generator.lr_dm_only, ndim=gan.ndim, sigma=gan.generator.sigma)

    batchsize = databatch.shape[0]
    
    z = singan_generator_noise(my_xlr.shape)
    batch_gan = [tf.constant(my_xlr), z]
    
    dummy_target = tf.zeros(batchsize)
    
    if gan.num_outputs > 1:
        target = [dummy_target]*gan.num_outputs
    else:
        target = dummy_target
        
    if gan.recloss_coeff > 0.:
        batch_gan.append(my_xhr)
        
    loss = gan.train_on_batch(batch_gan, target)
    return loss

def singan_train_step_critic(generator, wasserstein, databatch):
    
    ytrue = databatch
    xlr = hr_image_to_lr(ytrue, avg=generator.avg, lr_dm_only=generator.lr_dm_only, ndim=generator.ndim, sigma=generator.sigma)

    batchsize = xlr.shape[0]
    
    z = singan_generator_noise(xlr.shape)
    
    yfake = generator.predict([tf.constant(xlr), z])
    if wasserstein.input_lr_too:
        batch_wasserstein = [tf.constant(ytrue), tf.constant(yfake), tf.constant(xlr)]
    else:
        batch_wasserstein = [tf.constant(ytrue), tf.constant(yfake)]
    dummy_target = tf.zeros(batchsize, dtype=tf.float32)
    
    #print(dummy_target.shape)

    loss = wasserstein.train_on_batch(batch_wasserstein, (dummy_target, dummy_target, dummy_target))
    return loss


def singan_evaluate_metrics_batch(generator, wasserstein, databatch, noise=False):    
    ytrue = databatch
    xlr = hr_image_to_lr(ytrue, avg=generator.avg, lr_dm_only=generator.lr_dm_only, ndim=generator.ndim, sigma=generator.sigma)
    z = singan_generator_noise(xlr.shape, noise=noise)
    
    if (generator is None) or (wasserstein is None):
        metrics = {}
        # In this case write out the prediction error for the case
        # when no network was used
        metrics["mse"] = tf.reduce_mean(tf.square(xlr - ytrue), axis=(1,2,3))
        ymean = tf.reduce_mean(xlr - ytrue, axis=(1,2,3), keepdims=True)
        metrics["mse_zeromean"] = tf.reduce_mean(tf.square(xlr - ytrue - ymean), axis=(1,2,3))
        return metrics

    yfake = generator.predict([tf.constant(xlr), z])

    if wasserstein.input_lr_too:
        batch_wasserstein = [tf.constant(ytrue), tf.constant(yfake), tf.constant(xlr)]
    else:
        batch_wasserstein = [tf.constant(ytrue), tf.constant(yfake)]
    
    metrics = {}
    
    metrics["wasserstein"], metrics["gradient"], metrics["regularization"] = wasserstein.predict(batch_wasserstein)
    
    # custom metrics
    
    if generator.takelog:
        metrics["mse"] = tf.reduce_mean(tf.square(tf.math.log(yfake) - tf.math.log(ytrue)), axis=(1,2,3))
    else:
        metrics["mse"] = tf.reduce_mean(tf.square(yfake - ytrue), axis=(1,2,3))
    
    ymean = tf.reduce_mean(yfake - ytrue, axis=(1,2,3), keepdims=True)
    metrics["mse_zeromean"] = tf.reduce_mean(tf.square(yfake - ytrue - ymean), axis=(1,2,3))

    ytrue_np = ytrue.numpy()
    yfake_np = yfake

    ssim_met = []
    psnr_met = []

    for x in range(ytrue_np.shape[0]):
        true_img = ytrue_np[x,...,0]
        fake_img = yfake_np[x,...,0]
        img_ssim = structural_similarity(true_img, fake_img, data_range=1.0)
        ssim_met.append(img_ssim)

        img_psnr = peak_signal_noise_ratio(true_img, fake_img, data_range=1.0)
        psnr_met.append(img_psnr)

    metrics["ssim"] = ssim_met
    metrics["psnr"] = psnr_met
    #

    
    return metrics

def singan_evaluate_all_metrics(generator, wasserstein, data_val, noise=False, seed=42):
    ## validation
    
    if seed is not None:
        tf.random.set_seed(seed)
        state = np.random.get_state()
        np.random.seed(seed)
     
    metrics = {}

    for i,data_batch in enumerate(data_val):
        newmetrics = singan_evaluate_metrics_batch(generator, wasserstein, data_batch, noise=noise) 
        
        for key in newmetrics:
            if i == 0:
                metrics[key] = []
                
            # some metrics haven't bin averaged yet over the image dimension
            met_mean = np.mean(np.reshape(newmetrics[key], (len(data_batch), -1)), axis=-1)
            
            metrics[key].extend(met_mean)
        
    for key in metrics:
        metrics[key] = np.array(metrics[key])
        
    if seed is not None:
        np.random.set_state(state)
                    
    return metrics   


def singan_generate_samples(generator, xlr, seed=None, noise=True, noisescale=1., return_noisy=False):
    if seed is not None:
        tf.random.set_seed(seed)
        
    z = singan_generator_noise(xlr.shape, noise=noise)
    if generator is None:
        ygen = xlr
    else:
        ygen = generator.predict([xlr, z*noisescale])
    
    if return_noisy:
        return ygen, z*noisescale
    else:
        return ygen
    
