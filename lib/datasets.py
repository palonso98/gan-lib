import tensorflow as tf
import numpy as np
import utils as ut
from skimage.transform import downscale_local_mean

def downscale_image(im, subsamp, avg=False, random=False):
    if avg:
        #imnew = np.stack([downscale_local_mean(im[...,i],(subsamp,subsamp)) for i in range(0,im.shape[-1])], axis=-1)
        imnew = downscale_local_mean(im, (1,subsamp,subsamp,1))
    else:
        if random:
            ix, iy, iz = np.random.randint(0, subsamp, 3)
        else:
            ix, iy, iz = 0,0,0
        imnew = im[ix::subsamp, iy::subsamp, iz::subsamp]
    return imnew

def yield_random_patches_lag2d(datadir, database="1kev_", epochs=1, verbose=False, shuffle=True, random_transformations=True, patches_per_im=None, patchsize=48,
                         imlim=(0,512*3), subsamp=1, **kwargs):
    indices = np.arange(imlim[0], imlim[1])

    for epoch in range(0, epochs):
        if verbose:
            print("Starting epoch %d/%d" % (epoch, epochs))
        if shuffle:
            np.random.shuffle(indices)
    
        for ntot, ind in enumerate(indices):
            if (verbose >= 2) & (ntot % 1 == 0):
                print("E%d, Loading image %04d (%04d/%04d)" % (epoch,ind,ntot,imlim[1]))
            im = np.load("%s/%s%04d.npy" % (datadir, database, ind))
            
            if subsamp > 1:
                ims = ut.subsamp_fields(im, subsamp)
            else:
                ims = [im]
            
            patches = []
            for im in ims:
                if random_transformations:
                    shift = np.random.randint((0,0), im.shape[0:2])
                    im = np.roll(im, shift, axis=(0,1))

                    if np.random.uniform() > 0.5: # switch x and y
                        im = np.transpose(im, axes=(1,0,2))
                        im = np.roll(im, 1, axis=-1)

                    if np.random.uniform() > 0.5: # -x
                        im = im[::-1,:]
                        im[...,0] = - im[...,0]

                    if np.random.uniform() > 0.5: # -y
                        im = im[:,::-1]
                        im[...,1] = - im[...,1]
                        
                subpatches = ut.split_fields(im, chunk=(patchsize,patchsize))
                
                patches.append(subpatches)
                
            patches = np.array(patches)
            patches = np.reshape(patches, (-1,) + patches.shape[-3:])

            if (verbose>=1) & (ntot==0) & (epoch==0):
                print("Each Image has patches shape ", patches.shape)

            if patches_per_im is not None:
                patches = patches[np.random.randint(0, len(patches), patches_per_im)]

            for patch in patches:
                yield patch
                
def yield_random_patches(datadir, database="N768", epochs=1, verbose=False, shuffle=True, random_transformations=True, patches_per_im=32, patchsize=48, 
                         imlim=(0,100), subsamp=1, takelog=0, fileformat="npy"):
    
    indices = np.arange(imlim[0]+1, imlim[1]+1)#add or delete +1 depending on how you name your files
    p = patchsize
    for epoch in range(0, epochs):
        if verbose:
            print("Starting epoch %d/%d" % (epoch, epochs))
        if shuffle:
            np.random.shuffle(indices)
    
        for ntot, ind in enumerate(indices):
            if (verbose >= 2) & (ntot % 1 == 0):
                print("E%d, Loading image %04d (%04d/%04d)" % (epoch,ind,ntot,imlim[1]))
            if fileformat=="npy":
                im = np.load("%s/%s%04d.npy" % (datadir, database, ind))
            elif fileformat=="png":
                import imageio
                im = imageio.imread("%s%04d.png"%(datadir,ind))
                print("Reading... %s%04d.png"%(datadir,ind))
                im = im[...,np.newaxis]
                
            
            if takelog:
                im = np.clip(im,1e-2,None) 
                #im = np.log(im)
                
            if subsamp > 1:
                im_0 = downscale_local_mean(im[0],(subsamp,subsamp))
                im_1 = downscale_local_mean(im[1],(subsamp,subsamp))
                ims = [im_0,im_1]
                ims = np.moveaxis(ims,0,-1)
                ims = ims[np.newaxis,...]
            else:
                ims = im
                if fileformat=="npy":
                    ims = np.moveaxis(im,0,-1)
                ims = ims[np.newaxis,...]                
            patches = []
            
            for indim, im in enumerate(ims):
                maxrshift = np.min(( (np.array(im.shape[:-1]) - p)// 2, (p//2, p//2)), axis=0)
                
                if (np.min(maxrshift) < p//2) & (epoch==0) & (ntot==0) & (indim==0):
                    print("Warning can only randomly shift by", maxrshift, "but I'd prefer to shift by ", p//2)
                    print("The optimal randomization is achieved by a patchsize <= ", im.shape[0]//2)
                
                if random_transformations:
                    if np.min(maxrshift) > 0:
                        shift = np.random.randint(-maxrshift, maxrshift)
                        if fileformat=="npy":
                            im = np.roll(im, shift, axis=(0,1))[maxrshift[0]:-maxrshift[0], maxrshift[1]:-maxrshift[1]]
                        elif fileformat=="png":
                            im = np.roll(im, shift, axis=(0,1))[maxrshift[0]:-maxrshift[0], maxrshift[1]:-maxrshift[1]]

                    if np.random.uniform() > 1: 
                        im = im[::-1,:,:]

                    if np.random.uniform() > 0: 
                        im = im[:,::-1,:]
                             
                assert (im.shape[0] >= p) & (im.shape[1] >= p), "Please use a larger patchsize imsize=(%d,%d) versus p=%d" % (im.shape[0], im.shape[1], p)
                subpatches = ut.split_fields(im, chunk=(p,p),fileformat=fileformat)
                
                patches.append(subpatches)
                
            patches = np.array(patches)
            patches = np.reshape(patches, (-1,) + patches.shape[-3:]) 


            if (verbose>=1) & (ntot==0) & (epoch==0):
                print("Each Image has patches shape ", patches.shape)

            if patches_per_im is not None:
                patches = patches[np.random.randint(0, len(patches), patches_per_im)]
                
            #import bacco.visualization as vi
            #psmooth = vi.np_convolve_gaussian(patches[...,1], sigma=0.04, expand=(0.2,0.2))
            #patches[...,1] = psmooth

            for patch in patches:
                yield patch     

def yield_random_patches_lag3d(simdir, t, subsamp=2, cachedir=None, epochs=1, verbose=False, shuffle=True, random_transformations=True, patches_per_im=None, patchsize=48, imlim=(0,512*3), **kwargs):
    
    import time
    t0 = time.time()
    
    def rotate_vector_field(x, ax1=0, ax2=1):
        x[...,ax1], x[...,ax2] = np.copy(x[...,ax2]), np.copy(-x[...,ax1])
    
    import bacco.utils as bacco_ut
    
    pos, L = bacco_ut.cached_read_downsampled_posgrid(simdir=simdir, cachedir=cachedir, t=t, nsub=subsamp, displacement=True)
    
    indices = np.arange(imlim[0], imlim[1])
    
    np.random.seed(42)
    
    def random_transformation(im):
        shift = np.random.randint((0,0,0), im.shape[0:3])
        im = np.roll(im, shift, axis=(0,1,2))

        def rotate_vector_field(x,  ax1=0, ax2=1, k=1,):
            if k==0:
                return
            elif k==1:
                x[...,ax1], x[...,ax2] = np.copy(-x[...,ax2]), np.copy(x[...,ax1])
            elif k==2:
                x[...,ax1], x[...,ax2] = np.copy(-x[...,ax1]), np.copy(-x[...,ax2])
            elif k==3:
                x[...,ax1], x[...,ax2] = np.copy(x[...,ax2]), np.copy(-x[...,ax1])
            else:
                raise ValueError("k too large")

        k = np.random.randint(0,4)
        im = np.rot90(im, k=k, axes=(0, 1))
        rotate_vector_field(im, 0, 1, k=k)

        k = np.random.randint(0,4)
        im = np.rot90(im, k=k, axes=(0, 2))
        rotate_vector_field(im, 0, 2, k=k)

        k = np.random.randint(0,4)
        im = np.rot90(im, k=k, axes=(1, 2))
        rotate_vector_field(im, 1, 2, k=k)

        return im
    
    for epoch in range(0, epochs):
        if verbose:
            print("Starting epoch %d/%d" % (epoch, epochs))
        if shuffle:
            np.random.shuffle(indices)
            


            
        for ntot, ind in enumerate(indices):
            
            ims = [pos]
            
            #if subsamp > 1:
            #    ims = ut.subsamp_3Dfields(im, subsamp)
            #else:
            #    ims = [im]
            
            patches = []
            for im in ims:
                
                if random_transformations:
                    im = random_transformation(im)

                subpatches = ut.split_3Dfields(im, chunk=(patchsize,patchsize,patchsize))
                
                patches.append(subpatches)
                
            patches = np.array(patches)
            patches = np.reshape(patches, (-1,) + patches.shape[-4:])
           
            if (verbose>=1) & (ntot==0) & (epoch==0):
                print("Each Image has patches shape ", patches.shape)

            if patches_per_im is not None:
                patches = patches[np.random.randint(0, len(patches), patches_per_im)]

            for patch in patches:
                yield patch
                

def random_patch_dataset(patchsize=48, batchsize=32, randbuffer=1000, nchannels=2, yield_random_patches=yield_random_patches, ndim=2, **kwargs):
    output_shapes = (patchsize,)*ndim + (nchannels,)
    
    data = tf.data.Dataset.from_generator(lambda: yield_random_patches(patchsize=patchsize, **kwargs), 
                                          output_types=np.float32, output_shapes=output_shapes)
    if randbuffer > 1:
        data = data.shuffle(randbuffer).batch(batchsize).prefetch(tf.data.experimental.AUTOTUNE)
    else:
        data = data.batch(batchsize)
    
    return data


def define_datasets(hypar,basedir):
    """This function returns 3 tf dataset generators, one for the generator, one
    for the critic and one for validation purposes (this one should be independent of
    the first two)"""
    argdict = {}
    
    for key in ("subsamp", "batchsize", "patchsize", "takelog", "verbose", "randbuffer", "patches_per_im", "random_transformations"):
        if key in hypar:
            argdict[key] = hypar[key]
    
    yield_patches = yield_random_patches
    if "lag2d" in hypar["dataset"]:
        imlim_train = (0,256*5)
        imlim_test = (256*5,256*6)
        
        if "interp" in hypar["dataset"]:
            argdict["datadir"] = "%s/lag2dnew/interp_1024" % basedir
        else:
            argdict["datadir"] = "%s/lag2dnew/part_512" % basedir
            
        argdict["database"] = "1kev_"
        
        yield_patches = yield_random_patches_lag2d
        
        nchannels = 2
    elif "lag3d" in hypar["dataset"]:
        imlim_train = imlim_test = (0,10)
        
        assert "simdir" in hypar
        assert "snapind" in hypar
        
        yield_patches = yield_random_patches_lag3d
        
        argdict["t"] = hypar["snapind"]
        argdict["simdir"] = hypar["simdir"]
        argdict["cachedir"] = basedir + "/lag3d/cache"
        argdict["ndim"] = 3
        
        nchannels = 3
    elif "bio" in hypar["dataset"]:
        argdict["fileformat"]  = "png"
        nchannels = 1
        assert 0
    elif "dmhalo" in hypar["dataset"]:
        imlim_train = (0, 90)
        imlim_test = (90,100)
        
        if "200" in hypar["dataset"]:
            argdict["datadir"] = "%s/N1536_200" % (basedir,)
        elif "800" in hypar["dataset"]:
            argdict["datadir"] = "%s/N1536_800" % (basedir, )
        else:
            raise ValueError("Problem!")
        argdict["database"] = "N1536_"
        argdict["fileformat"]  = "npy"
        nchannels = 2
    else:
        raise ValueError("Unknown dataset " + hypar["dataset"])
        
    data_gen = random_patch_dataset(imlim=imlim_train, epochs=500, nchannels=nchannels, yield_random_patches=yield_patches, **argdict)
    data_cri = random_patch_dataset(imlim=imlim_train, epochs=500, nchannels=nchannels, yield_random_patches=yield_patches,**argdict)
    data_val = random_patch_dataset(imlim=imlim_test, epochs=1, nchannels=nchannels, yield_random_patches=yield_patches,**argdict)
    
    return data_gen, data_cri, data_val

# patchers_per_im 32-> None
# randbuffer 5000->100