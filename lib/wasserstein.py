import tensorflow as tf
import tensorflow.keras.layers as layers
import numpy as np
from tensorflow.keras import backend as K

assert False, "Don't use this module now, it is deprecated. Use singan instead, it is also a wasserstein gan!"

def dense_block(inputs, n, alpha=0.2):
    x = layers.Dense(n)(inputs)
    #x = layers.LeakyReLU(alpha=alpha)(x)
    return x

def define_generator(patchsize=8, name="generator", nchan=2, d=64, spectral_norm=True, subtractmean=True):
    xhr = tf.keras.Input(shape=(6*patchsize, 6*patchsize, nchan))
    xlr = tf.keras.Input(shape=(patchsize, patchsize, nchan))
    
    z1 = tf.keras.Input(shape=(4*(6*(patchsize // 8))**2 ,))
    z2 = tf.keras.Input(shape=((patchsize, patchsize, 1)))
    
    inputs = [xlr, xhr, z1, z2]
    
    # xhr 48,48,2
    # xlr 8,8,2
    # z1 6**2
    # z2 8**2,1
    
    if subtractmean:
        x0 = layers.GlobalAveragePooling2D()(xlr)[...,tf.newaxis,tf.newaxis,:]
    else:
        x0 = tf.constant((0.,0.,0.))
    
    x = xhr - x0
    
    x = conv_block(x, d, strides=2, spectral_norm=spectral_norm)
    x = conv_block(x, d, strides=2, spectral_norm=spectral_norm)
    x = conv_block(x, 4, strides=2, spectral_norm=spectral_norm)
    
    x = layers.Flatten()(x)
    
    x = layers.Concatenate(axis=-1)([x,z1])
    
    n = 4
    x = dense_block(x, n*patchsize**2)
    
    x = layers.Reshape((patchsize, patchsize, n))(x)
    
    x = layers.Concatenate(axis=-1)([x,xlr-x0, z2])
    
    x = conv_block(x, 8*d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, 2*d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, 2*d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    
    x = conv_block(x, 4*d, strides=2, upconv=True, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, 4*d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, nchan, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    
    model = tf.keras.Model(inputs=inputs, outputs=x+x0, name=name)
    model.patchsize = patchsize
    model.nchan = nchan
    
    return model

def define_critic(patchsize=8, nchan=2, name="critic", d=32, subtractmean=True, spectral_norm=True):
    xlr = tf.keras.Input(shape=(3*patchsize, 3*patchsize, nchan))
    xhr = tf.keras.Input(shape=(6*patchsize, 6*patchsize, nchan))
    y = tf.keras.Input(shape=(2*patchsize, 2*patchsize, nchan))
    
    inputs = [xlr, xhr, y]
    
    if subtractmean:
        x0 = layers.GlobalAveragePooling2D()(xlr)[...,tf.newaxis,tf.newaxis,:]
    else:
        x0 = tf.constant((0.,0.,0.))
    
    ypad = layers.ZeroPadding2D(padding=(2*patchsize, 2*patchsize))(y)
    x = (xhr + ypad) - x0
    
    xlrup = layers.UpSampling2D(size=(2,2))(xlr- x0)
    
    x = layers.Concatenate(axis=-1)([x, xlrup, x-xlrup])
    
    x = conv_block(x, d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, 2*d, strides=2, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, 4*d, strides=2, activation="leaky_relu", spectral_norm=spectral_norm)
    
    x = conv_block(x, d, strides=1, activation="leaky_relu", spectral_norm=spectral_norm)
    x = conv_block(x, d, strides=2, activation="leaky_relu", spectral_norm=spectral_norm)
    
    x = layers.Flatten()(x)
    x = dense_block(x, 1)
    
    model = tf.keras.Model(inputs=inputs, outputs=x, name=name)
    
    # save some hyperparameters
    model.patchsize = patchsize
    model.nchan = nchan
    
    return model

def wasserstein_metric_model(critic, optimizer="rmsprop"):
    assert 0, "This function is depreceated, use the class WassersteinGPModel below"
    
    self.critic = critic
    
    patchsize = critic.patchsize=8
    nchan = critic.nchan
    
    ytrue = tf.keras.Input(shape=(2*patchsize, 2*patchsize, nchan))
    yfake = tf.keras.Input(shape=(2*patchsize, 2*patchsize, nchan))
    xhr = tf.keras.Input(shape=(6*patchsize, 6*patchsize, nchan))
    xlr = tf.keras.Input(shape=(3*patchsize, 3*patchsize, nchan))
    
    inputs = [xlr, xhr, ytrue, yfake]
    
    Dtrue = critic([xlr, xhr, ytrue])
    Dfake = critic([xlr, xhr, yfake])
    
    ws_metric = Dtrue - Dfake
    
    #Dgrad = tf.gradients(ws_metric, ytrue)
    #print(Dgrad)
    
    #with tf.GradientTape(persistent=True) as tape:
    #    tape.watch(ytrue)
    #    Dtrue2 = tf.reduce_mean(critic([xlr, xhr, ytrue]))
        
    #grad = tape.gradient(Dtrue2, ytrue)
    
    #print(grad)

    
    
    
    model = tf.keras.Model(inputs=inputs, outputs=ws_metric)
    
    def myloss(y_true, y_pred):
        #print(tape)
        #grad2 = tf.reduce_sum(tf.square(tape.gradient(Dtrue2, ytrue)), axis=(-1,-2,-3))
        #print(grad)
        # need to have y_true as argument, allthough we are not gonna use it
        return -tf.reduce_mean(y_pred) #+ tf.reduce_mean(tf.square(tf.sqrt(grad2) - 1.))
    
    model.compile(loss=myloss, optimizer=optimizer)
    
    return model

def gan_model(generator, critic, optimizer="rmsprop"):    
    patchsize = critic.patchsize
    nchan = critic.nchan
    
    xhr = tf.keras.Input(shape=(6*patchsize, 6*patchsize, nchan))
    xlr = tf.keras.Input(shape=(3*patchsize, 3*patchsize, nchan))
    xlr_center = tf.keras.Input(shape=(patchsize, patchsize, nchan))
    
    z1 = tf.keras.Input(shape=(4*(6*(patchsize // 8))**2 ,))
    z2 = tf.keras.Input(shape=((patchsize, patchsize, 1)))
    
    inputs = [xlr, xlr_center, xhr, z1, z2]
    
    # connect them
    yfake = generator([xlr_center, xhr, z1, z2])
    wsfake = critic([xlr, xhr, yfake])
    
    model = tf.keras.Model(inputs=inputs, outputs=wsfake)
    
    def myloss(y_true, y_pred):
        # need to have y_true as argument, allthough we are not gonna use it
        return -tf.reduce_mean(y_pred) # minimize wasserstein_metric
    
    model.compile(loss=myloss, optimizer=optimizer)
    
    model.iteration = tf.Variable(0, dtype=tf.int64, trainable=False)
    
    model.generator = generator
    model.critic = critic
    
    return model

def combine_center(s, scent):
    assert (s.shape[-3] % 3 == 0) & (s.shape[-2] % 3 == 0), s.shape
    
    noff = np.array(s.shape[-3:-1])//3

    snew = np.copy(s)
    snew[...,noff[0]:2*noff[0], noff[1]:2*noff[1],:] = scent
    return snew

def generator_noise(nbatch, patchsize=8, noise=True):
    if noise:
        z1 = tf.random.normal(shape=(nbatch, 4*(6*(patchsize // 8))**2 ,))
        z2 = tf.random.normal(shape=(nbatch, patchsize, patchsize, 1))
    else:
        z1 = tf.zeros(shape=(nbatch, 4*(6*(patchsize // 8))**2 ,))
        z2 = tf.zeros(shape=(nbatch, patchsize, patchsize, 1))
    
    return z1, z2

def generate_samples(generator, xlr_center_input, xhr_input, seed=None, noise=True):
    if seed is not None:
        tf.random.set_seed(seed)
        
    nbatch = xlr_center_input.shape[0]
    patchsize = generator.patchsize
      
    z1, z2 = generator_noise(nbatch, patchsize, noise=noise)
    
    ygen = generator.predict([xlr_center_input, xhr_input, z1, z2])
    
    ygc = combine_center(xhr_input, ygen)
    
    return ygc

class WassersteinGPModel(tf.keras.models.Model):
    def __init__(self, critic):
        super(WassersteinGPModel, self).__init__()
        self.critic = critic

    @tf.function
    def call(self, input_data):
        xlr, xhr, ytrue, yfake = input_data

        # wasserstein metric
        Dtrue = self.critic([xlr, xhr, ytrue])
        Dfake = self.critic([xlr, xhr, yfake])
        
        Dprod = layers.ReLU()(Dtrue * Dfake)
        
        ws_metric = Dtrue - Dfake
        
        # gradient penalty
        # create a random image somewhere between true and fake ones to test the gradients
        # at random locations
        alpha = tf.random.uniform(shape=(1,), minval=0, maxval=1.)
        yalpha = alpha * ytrue + (alpha-1.) * yfake
        
        Dalpha = self.critic([xlr, xhr, yalpha])
        grad = tf.gradients(Dalpha, yalpha)
        
        # Note that [0] is there because tensorflow reshapes to (1,...) in the beginning for some unknown reason...
        gradabs = tf.sqrt(tf.reduce_mean(tf.square(grad), axis=(-1,-2,-3)), name="myloss")[0]

        return ws_metric, gradabs, Dprod
    
    def get_loss_functions(self, lam=10., stabilization_loss=0.):
        def loss_wsm(y_true, y_pred):
            wsmetric = y_pred
            return  -tf.math.reduce_mean(wsmetric)
        
        def loss_gp(y_true, y_pred):
            gradabs = y_pred
            gradient_panelty = lam*tf.math.reduce_mean(tf.square(gradabs - 1.))

            return gradient_panelty
        
        def loss_stabilization(y_true, y_pred):
            return stabilization_loss*y_pred
        
        return [loss_wsm, loss_gp, loss_stabilization]
    


class SpectNormLayer(layers.Conv2D):
    
    def __init__(self, *args, **kwargs):
        super(SpectNormLayer, self).__init__(*args, **kwargs)
    
    def call(self, inputs):
        out = super(SpectNormLayer, self).call(inputs)
    

        return out
    
class SpectNormLayerTranspose(layers.Conv2DTranspose):
    
    def __init__(self, *args, **kwargs):
        super(SpectNormLayerTranspose, self).__init__(*args, **kwargs)

    def call(self, inputs):
        out = super(SpectNormLayerTranspose, self).call(inputs)
    
        #weights_norm = spectral_norm(self.weights, iteration=1)
        #self.weights[0] = self.weights[0]+2.
        #import ipdb; ipdb.set_trace()

        return out
    


def train_step_critic(generator, wasserstein, databatch):
    my_xlr_full, my_xlr_center, my_xhr_input, my_y = databatch
    batchsize = my_xlr_full.shape[0]
    
    z1, z2 = generator_noise(batchsize, patchsize=generator.patchsize)
    batch_gen = [my_xlr_center, my_xhr_input, z1, z2]

    yfake = generator.predict(batch_gen)
    batch_wasserstein = [tf.constant(my_xlr_full), tf.constant(my_xhr_input), tf.constant(my_y), tf.constant(yfake)]
    dummy_target = tf.zeros(batchsize, dtype=tf.float32)
    
    loss = wasserstein.train_on_batch(batch_wasserstein, (dummy_target, dummy_target, dummy_target))
    return loss

def evaluate_metrics_batch(generator, wasserstein, databatch, noise=False):
    my_xlr_full, my_xlr_center, my_xhr_input, my_y = databatch
    batchsize = my_xlr_full.shape[0]
    
    z1, z2 = generator_noise(batchsize, patchsize=generator.patchsize, noise=noise)
    batch_gen = [my_xlr_center, my_xhr_input, z1, z2]

    yfake = generator.predict(batch_gen)
    batch_wasserstein = [tf.constant(my_xlr_full), tf.constant(my_xhr_input), tf.constant(my_y), tf.constant(yfake)]
    
    metrics = {}
    
    metrics["wasserstein"], metrics["gradient"], metrics["regularization"] = wasserstein.predict(batch_wasserstein)
    
    # custom metrics
    metrics["mse"] = tf.reduce_mean(tf.square(yfake - my_y), axis=(1,2,3))
    #
    
    return metrics

def evaluate_all_metrics(generator, wasserstein, data_val, noise=False, seed=42):
    ## validation
    
    if seed is not None:
        state = np.random.get_state()
        np.random.seed(seed)
        
    iter_val = iter(data_val)
     
    metrics = {}
    #metrics = {"wasserstein": [], "gradient": [], "regularization": [], "mse"}
    for i,data_batch in enumerate(data_val):
        newmetrics = evaluate_metrics_batch(generator, wasserstein, data_batch)
        #print(len(loss), loss[0].shape, loss[1].shape, loss[2].shape)
        #metrics["wasserstein"].extend(newmetrics[0][...,0])
        #metrics["gradient"].extend(newmetrics[1])
        #metrics["regularization"].extend(newmetrics[2][...,0])
        
            
        
        for key in newmetrics:
            if i == 0:
                metrics[key] = []
            
            metrics[key].extend(np.reshape(newmetrics[key], -1))
        
    for key in metrics:
        metrics[key] = np.array(metrics[key])
        
    if seed is not None:
        np.random.set_state(state)
                    
    return metrics    

def train_step_generator(gan, databatch):
    my_xlr_full, my_xlr_center, my_xhr = databatch
    batchsize = my_xlr_full.shape[0]
    
    z1, z2 = generator_noise(batchsize, patchsize=gan.generator.patchsize)
    batch_gan = [my_xlr_full, my_xlr_center, my_xhr, z1, z2]
    dummy_target = np.zeros(batchsize)
    
    loss = gan.train_on_batch(batch_gan, dummy_target)
    return loss