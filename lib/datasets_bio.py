import tensorflow as tf
import numpy as np
from skimage import io
from skimage.util import img_as_ubyte
from skimage import transform
from skimage import filters
import os


def yield_bio_patch_dataset(filenames, patch_width, patch_height, num_x_patches, num_y_patches, data_augmentation = True, epochs=1, seed=42):
    
    rng = np.random.default_rng(seed)

    for i in range(0, epochs):
        for x in filenames:
            im =  img_as_ubyte( io.imread( x ) )
            if data_augmentation:
                # Create 1 less row of patches per dimension, but start the patch "grid" with a random displacement from [0,0]
                startx = rng.integers(patch_width)
                starty = rng.integers(patch_height)
                rangex = range(num_x_patches - 1)
                rangey = range(num_y_patches - 1)
            else:
                rangex = range(num_x_patches)
                rangey = range(num_y_patches)

            patches = []
            for i in rangex:
                for j in rangey:
                    if data_augmentation:
                        patch = im[ startx + (i * patch_width) : startx + ((i+1) * patch_width), starty + (j * patch_height) : starty + ((j+1) * patch_height) ]
                    else:
                        patch = im[ i * patch_width : (i+1) * patch_width, j * patch_height : (j+1) * patch_height ]
                    patch = np.reshape(patch, (patch_width, patch_height, 1))
                    patch = patch / 255
                    if data_augmentation:
                        patch = transform.rotate(patch, 90*rng.integers(0,5), preserve_range=True, mode='reflect')
                        if rng.random() < 0.5:
                            patch = np.flip(patch, 0)
                        if rng.random() < 0.5:
                            patch = np.flip(patch, 1)
                    patches.append(patch)
            
            for p in patches:
                yield p
        

def bio_patch_dataset(filenames, num_x_patches, num_y_patches, data_augmentation = True, randbuffer = 100, batchsize=8, epochs=1, seed=42):

    im =  img_as_ubyte( io.imread( filenames[0] ) ) # Get image size (all images are same size)

    original_size = im.shape

    patch_width = original_size[ 0 ] // num_x_patches
    patch_height = original_size[ 1 ] // num_y_patches

    data = tf.data.Dataset.from_generator(lambda: yield_bio_patch_dataset(filenames, patch_width, patch_height, num_x_patches, num_y_patches, epochs=epochs, seed=42),
                                          output_types=np.float32, output_shapes=(patch_width, patch_height, 1))

    if randbuffer > 1:
        data = data.shuffle(randbuffer, seed=seed).batch(batchsize).prefetch(tf.data.experimental.AUTOTUNE)
    else:
        data = data.batch(batchsize)
    
    return data

def bio_define_datasets(hypar, basedir):
    filenames = [basedir + '/' + x for x in os.listdir( basedir ) if x.endswith(".png")]
    filenames.sort()

    validation_amount = int(hypar["validation_split"] * len(filenames))
    
    validation_files = filenames[0:validation_amount]
    training_files = filenames[validation_amount:-1]

    # TODO: SEED
    num_x_patches = hypar["num_x_patches"]
    num_y_patches = hypar["num_y_patches"]
    data_augmentation = hypar["data_augmentation"]
    data_val = bio_patch_dataset(validation_files, num_x_patches, num_y_patches, data_augmentation, epochs=1, randbuffer=0)
    data_gen = bio_patch_dataset(training_files, num_x_patches, num_y_patches, data_augmentation, epochs=500, seed=1)
    data_cri = bio_patch_dataset(training_files, num_x_patches, num_y_patches, data_augmentation, epochs=500, seed=1)

    return data_gen, data_cri, data_val

def hr_to_lr(hr_imgs, down_factor=2, sigma=3):
    lr = np.copy(hr_imgs)

    for i in range(hr_imgs.shape[0]):
        hr_img = hr_imgs[i]
        hr_blur = filters.gaussian(hr_img, sigma=sigma) + 1e-6

        lr_width = hr_img.shape[0] // down_factor
        lr_height = hr_img.shape[1] // down_factor

        lr_img = transform.resize(hr_blur, (lr_width, lr_height), order=1)

        for n in (0,1):
            for m in (0,1):
                lr[i,n::2,m::2,:]=lr_img

    return np.array(lr)

def hr_to_lr_reduce_size(hr_imgs, down_factor=2, sigma=3):
    lrs = []

    for i in range(hr_imgs.shape[0]):
        hr_img = hr_imgs[i]
        hr_blur = filters.gaussian(hr_img, sigma=sigma) + 1e-6

        lr_width = hr_img.shape[0] // down_factor
        lr_height = hr_img.shape[1] // down_factor

        lr_img = transform.resize(hr_blur, (lr_width, lr_height), order=1)
        lrs.append(lr_img)

    return np.array(lrs)
